#!/usr/bin/env bash

set -euo pipefail  # https://gist.github.com/maxisam/e39efe89455d26b75999418bf60cf56c

docker run --name shares-profit-calc\
    --init\
    --restart unless-stopped\
    -v "/etc/localtime:/etc/localtime:ro"\
    --mount type=bind,source="$(pwd)/config",target=/home/app/config,readonly \
    -d\
    shares-profit-calc:latest
