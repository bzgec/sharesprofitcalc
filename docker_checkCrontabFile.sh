#!/bin/sh

# Check if we have new crontab file.
# If so update the default crontab file ("/etc/crontab").

HOME_DIR=/home/app
FILE_OLD=/tmp/crontab.txt.old
FILE_NEW=$HOME_DIR/config/crontab.txt

if ! cmp --silent $FILE_OLD $FILE_NEW; then
    cp $FILE_NEW $FILE_OLD
    cp $FILE_NEW /etc/crontab
    printf "crontab.txt updated"
    date '+ - %Y/%m/%d %H:%M:%S'
fi
