import os
import tempfile
import unittest
from datetime import date
from unittest.mock import MagicMock

import src.currency_conversion as cc


def setup_db_file() -> str:
    """Create db file"""
    with tempfile.NamedTemporaryFile(
        mode="w", prefix="currency-conv-test-usd", newline="", delete=False
    ) as file_write:
        file_write.write(
            """Date;EUR-to-USD
2023-11-08;1.0671
2023-11-09;1.0691
2023-11-10;1.0683
2023-11-13;1.067
2023-11-14;1.0724
2023-11-15;1.0868
2023-11-20;1.0928
2023-11-21;1.0955
2023-11-22;1.0911
2023-11-23;1.09
2023-11-24;1.0916"""
        )

        return file_write.name


def setup_db_data() -> [(str, str)]:
    return [
        ("2023-11-08", "1.0671"),
        ("2023-11-09", "1.0691"),
        ("2023-11-10", "1.0683"),
        ("2023-11-13", "1.067"),
        ("2023-11-14", "1.0724"),
        ("2023-11-15", "1.0868"),
        ("2023-11-20", "1.0928"),
        ("2023-11-21", "1.0955"),
        ("2023-11-22", "1.0911"),
        ("2023-11-23", "1.09"),
        ("2023-11-24", "1.0916"),
    ]


class TestStr(unittest.TestCase):
    def test_setup_link_currency(self):
        """Test setup_link_dates"""

        # Link should be according to https://data.ecb.europa.eu/help/api/data
        # It should specify daily, average of the day, base currency should be EUR, CSV file
        self.assertEqual(
            "https://data-api.ecb.europa.eu/service/data/EXR/D.USD.EUR.SP00.A?format=csvdata",
            cc.setup_link_currency("USD"),
        )

    def test_setup_link_dates(self):
        """Test setup_link_dates"""
        date_start = date(2021, 5, 28)
        date_end = date(2023, 11, 23)

        link_start = "some_link"

        # Start and end date should be according to https://data.ecb.europa.eu/help/api/data
        self.assertEqual(
            link_start + "&startPeriod=2021-05-28&endPeriod=2023-11-23",
            cc.setup_link_dates(link_start, date_start, date_end),
        )

    def test_remove_unused_data(self):
        """Test remove_unused_data"""

        file_orig = ""
        file_mod = ""

        # Create original file
        with tempfile.NamedTemporaryFile(
            mode="w", prefix="currency-conv-test-usd", newline="", delete=False
        ) as file_write:
            file_write.write(
                """KEY,FREQ,CURRENCY,CURRENCY_DENOM,EXR_TYPE,EXR_SUFFIX,TIME_PERIOD,OBS_VALUE,OBS_STATUS,OBS_CONF,OBS_PRE_BREAK,OBS_COM,TIME_FORMAT,BREAKS,COLLECTION,COMPILING_ORG,DISS_ORG,DOM_SER_IDS,PUBL_ECB,PUBL_MU,PUBL_PUBLIC,UNIT_INDEX_BASE,COMPILATION,COVERAGE,DECIMALS,NAT_TITLE,SOURCE_AGENCY,SOURCE_PUB,TITLE,TITLE_COMPL,UNIT,UNIT_MULT
EXR.D.USD.EUR.SP00.A,D,USD,EUR,SP00,A,2009-05-04,1.3223,A,,,,P1D,,A,,,,,,,,,,4,,4F0,,US dollar/Euro,"ECB reference exchange rate, US dollar/Euro, 2:15 pm (C.E.T.)",USD,0
EXR.D.USD.EUR.SP00.A,D,USD,EUR,SP00,A,2009-05-05,1.3403,A,,,,P1D,,A,,,,,,,,,,4,,4F0,,US dollar/Euro,"ECB reference exchange rate, US dollar/Euro, 2:15 pm (C.E.T.)",USD,0"""
            )

            file_orig = file_write.name

        file_mod = file_orig + "mod"

        cc.remove_unused_data(file_orig, file_mod, "USD")

        file_data_mod = ""
        with open(file_mod, "r") as file_read:
            file_data_mod = file_read.read()

        # File is formatted according to https://data.ecb.europa.eu/help/api/data
        self.assertEqual(
            "Date;EUR-to-USD\n2009-05-04;1.3223\n2009-05-05;1.3403\n", file_data_mod
        )

        os.remove(file_orig)
        os.remove(file_mod)

    def test_update_db_currency_not_supported(self):
        """Test that exception is raised when currency is not supported"""
        with self.assertRaises(cc.UnsupportedCurrencyError) as cm:
            date_start = date(2021, 5, 28)
            cc.update_db("db-X.csv", "X", date_start)

        self.assertEqual('Currency "X" is not supported', str(cm.exception))

    def test_update_db_bad_db_name(self):
        """Test that db name ends with '.csv'"""
        with self.assertRaises(cc.UnsupportedDatabaseNameError) as cm:
            date_start = date(2021, 5, 28)
            cc.update_db("db-USD.cs", "USD", date_start)

        self.assertEqual('Database name must end with ".csv"', str(cm.exception))

    def test_update_db_no_old_db(self):
        """Test what to do when there is no old database"""
        cc.setup_link_currency = MagicMock(return_value="https://some-link-GBP")
        cc.setup_link_dates = MagicMock(return_value="https://some-link-GBP")
        cc.download_csv = MagicMock()
        cc.remove_unused_data = MagicMock()

        date_start = date(2021, 5, 28)
        cc.update_db("db-GBP.csv", "GBP", date_start)

        cc.setup_link_currency.assert_called_once_with("GBP")
        cc.setup_link_dates.assert_called_once_with(
            "https://some-link-GBP", date_start, date.today()
        )
        cc.download_csv.assert_called_once_with(
            "https://some-link-GBP", "db-GBP-orig.csv"
        )
        cc.remove_unused_data.assert_called_once_with(
            "db-GBP-orig.csv", "db-GBP.csv", "GBP"
        )

    def test_get_conversion_rate_db_currency_not_supported(self):
        """Test that exception is raised when currency is not supported"""
        with self.assertRaises(cc.UnsupportedCurrencyError) as cm:
            cc.get_conversion_rate_db("db-X.csv", "X")

        self.assertEqual('Currency "X" is not supported', str(cm.exception))

    def test_get_conversion_rate_db(self):
        """Test that correct database is returned"""
        file_mod = setup_db_file()

        self.assertEqual(setup_db_data(), cc.get_conversion_rate_db(file_mod, "USD"))

        os.remove(file_mod)

    def test_get_conversion_rate_date_out_of_range(self):
        """Test when currency conversion date is out of range"""

        db_usd = setup_db_data()

        # Date too small
        with self.assertRaises(cc.DateOutOfRangeError) as cm:
            self.assertEqual(0.0, cc.get_conversion_rate(db_usd, date(2023, 11, 7)))

        self.assertEqual(
            "Date 2023-11-07 is lower than the oldest date stored in file (2023-11-08)",
            str(cm.exception),
        )

        # Date too big (bigger than 2 weeks - no reason for this specific number,
        # just that it should be bigger than biggest expected number of sequential days
        # that currency conversion was not recorded)
        # (there is no currency conversion data on weekends and holidays...)
        with self.assertRaises(cc.DateOutOfRangeError) as cm:
            self.assertEqual(0.0, cc.get_conversion_rate(db_usd, date(2023, 12, 9)))

        self.assertEqual(
            "Date 2023-12-09 is 14+ days bigger than the newest date stored in file (2023-11-24)",
            str(cm.exception),
        )

    def test_get_conversion_rate_date_found(self):
        """Test when currency conversion date is found"""

        db_usd = setup_db_data()

        # Min
        self.assertEqual(1.0671, cc.get_conversion_rate(db_usd, date(2023, 11, 8)))

        # Middle
        self.assertEqual(1.0911, cc.get_conversion_rate(db_usd, date(2023, 11, 22)))

        # Max - stored
        self.assertEqual(1.0916, cc.get_conversion_rate(db_usd, date(2023, 11, 24)))

    def test_get_conversion_rate_date_not_found(self):
        """Test when currency conversion date is found"""

        db_usd = setup_db_data()

        self.assertEqual(1.0868, cc.get_conversion_rate(db_usd, date(2023, 11, 19)))
        self.assertEqual(1.0868, cc.get_conversion_rate(db_usd, date(2023, 11, 18)))
        self.assertEqual(1.0868, cc.get_conversion_rate(db_usd, date(2023, 11, 17)))
        self.assertEqual(1.0868, cc.get_conversion_rate(db_usd, date(2023, 11, 16)))

        # Max - allowed without error (14 days)
        self.assertEqual(1.0916, cc.get_conversion_rate(db_usd, date(2023, 12, 8)))
