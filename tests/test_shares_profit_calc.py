"""
https://docs.python.org/3/library/exceptions.html
https://docs.python.org/3/library/unittest.html
https://docs.python.org/3/library/unittest.mock.html
"""
import unittest
from datetime import date
from unittest.mock import call, patch
import sys
import os
import io
from contextlib import redirect_stderr

from src.shares_profit_calc import Currency, SharesProfit, SharesToUse


def fake_get_latest_stock_price(*args, **kwargs):
    if args[0] == "MSFT":
        return 200
    elif args[0] == "GOOGL":
        return 100
    elif args[0] == "AAPL":
        raise Exception("AAPL is sold share - don't scrape it's price")
    else:
        raise Exception("DON'T SCRAPE sold shares")


def fake_get_latest_stock_price_zero(*args, **kwargs):
    if args[0] == "MSFT":
        return 0
    elif args[0] == "GOOGL":
        return 100
    else:
        raise Exception("DON'T SCRAPE sold shares")


def fake_get_latest_stock_price_error(*args, **kwargs):
    if args[0] == "MSFT":
        return 200
    elif args[0] == "GOOGL":
        raise TypeError("string indices must be integers, not 'str'")
    else:
        raise Exception("DON'T SCRAPE sold shares")


def fake_get_conversion_rate_db(db_mod: str, currency: str) -> [(str, str)]:
    if currency == "USD":
        return [("dateUSD", "conv_ratioUSD")]
    elif currency == "GBP":
        return [("dateGBP", "conv_ratioGBP")]
    else:
        raise Exception("Bad conversion rate scraping")


def fake_get_conversion_rate1(db_data: [(str, str)], conv_date: date) -> float:
    # Note that `db_data` is different in real function
    if db_data[0] == "USD-db":
        if conv_date == date(2021, 5, 28):
            return 2
        elif conv_date == date(2022, 5, 28):
            return 2.5
        else:
            raise Exception(f"Wrong date: {conv_date} for {db_data}")
    elif db_data[0] == "GBP-db":
        if conv_date == date(2021, 5, 28):
            return 2.1
        elif conv_date == date.today():
            return 4
        else:
            raise Exception(f"Wrong date: {conv_date} for {db_data}")
    else:
        raise Exception(f"Wrong db_data: {db_data}")


def fake_get_conversion_rate2(db_data: [(str, str)], conv_date: date) -> float:
    # Note that `db_data` is different in real function
    if db_data[0] == "USD-db":
        if conv_date == date(2021, 5, 28):
            return 2
        elif conv_date == date.today():
            return 2.5
        else:
            raise Exception(f"Wrong date: {conv_date} for {db_data}")
    elif db_data[0] == "GBP-db":
        if conv_date == date(2021, 5, 28):
            return 2.1
        elif conv_date == date.today():
            return 4
        else:
            raise Exception(f"Wrong date: {conv_date} for {db_data}")
    else:
        raise Exception(f"Wrong db_data: {db_data}")


class TestSharesProfitCalcInit(unittest.TestCase):
    def setUp(self):
        self.shares_list = [
            {
                "symbol": "AAPL",
                "company": "Apple Inc.",
                "numb_of_shares": 3,
                "currency": "USD",
                "price_buy": 100,
                "price_sell": 150,
                "date_buy": "2021/05/28",
                "date_sell": "2022/05/28",
                "commission_buy": 0,
                "commission_sell": 1,
                "cap_gain_tax": 27.5,
            },
            {
                "symbol": "MSFT",
                "company": "Microsoft Corporation",
                "numb_of_shares": 2,
                "currency": "USD",
                "price_buy": 210,
                "price_sell": 0,
                "date_buy": "2021/05/28",
                "date_sell": "",
                "commission_buy": 2,
                "commission_sell": 0,
                "cap_gain_tax": 27.5,
            },
            {
                "symbol": "GOOGL",
                "company": "Alphabet Inc. Cl A",
                "numb_of_shares": 1,
                "currency": "EUR",
                "price_buy": 90,
                "price_sell": 0,
                "date_buy": "2021/05/28",
                "date_sell": "",
                "commission_buy": 1,
                "commission_sell": 3,
                "cap_gain_tax": 25,
            },
            {
                "symbol": "GOOGL",
                "company": "Alphabet Inc. Cl A",
                "numb_of_shares": 1,
                "currency": "EUR",
                "price_buy": 95,
                "price_sell": 0,
                "date_buy": "2021/05/29",
                "date_sell": "",
                "commission_buy": 1,
                "commission_sell": 3,
                "cap_gain_tax": 25,
            },
        ]

        self.mail_config = {
            "sender_mail": "sender@gmail.com",
            "sender_pass": "password",
            "send_to": "recipient@gmail.com",
        }

    def test_noShares(self):
        """Test that exception is raised when shares is empty array"""
        shares = []
        with self.assertRaises(AttributeError):
            SharesProfit(shares)

    def test_okNoMailConfig(self):
        """Test that no exception is raised when mail config is empty"""
        with patch(
            "src.shares_profit_calc.SharesProfit.get_currency_conversion_rates"
        ) as mock_get_currency_conversion_rates:
            with patch(
                "src.shares_profit_calc.SharesProfit.get_end_stock_price"
            ) as mock_get_end_stock_price:
                SharesProfit(
                    self.shares_list,
                    mail_config=self.mail_config,
                )

                # Remove unused warnings
                _ = mock_get_currency_conversion_rates
                _ = mock_get_end_stock_price

    def test_okMailConfig(self):
        """Test mail_sender.Mail() is called with correct arguments"""
        with patch(
            "src.shares_profit_calc.SharesProfit.get_currency_conversion_rates"
        ) as mock_get_currency_conversion_rates:
            with patch(
                "src.shares_profit_calc.SharesProfit.get_end_stock_price"
            ) as mock_get_end_stock_price:
                with patch(
                    "src.shares_profit_calc.mail_sender.Mail"
                ) as mock_mail_sender:
                    SharesProfit(
                        self.shares_list,
                        mail_config=self.mail_config,
                    )
                    mock_mail_sender.assert_called_once_with(
                        self.mail_config["sender_mail"], self.mail_config["sender_pass"]
                    )

                # Remove unused warnings
                _ = mock_get_currency_conversion_rates
                _ = mock_get_end_stock_price

    def test_mailConfig_senderMailMissing(self):
        """Test that exception is raised when mail config is wrong"""
        mail_config = {
            "sender_pass": "sender_gmail_pass",
            "send_to": "recipient.mail@gmail.com",
        }

        with self.assertRaises(AttributeError):
            SharesProfit(self.shares_list, mail_config=mail_config)

    def test_mailConfig_senderPassMissing(self):
        """Test that exception is raised when mail config is wrong"""
        mail_config = {
            "sender_mail": "sender.gmail@gmail.com",
            "send_to": "recipient.mail@gmail.com",
        }

        with self.assertRaises(AttributeError):
            SharesProfit(self.shares_list, mail_config=mail_config)

    def test_mailConfig_sendToMissing(self):
        """Test that exception is raised when mail config is wrong"""
        mail_config = {
            "sender_mail": "sender.gmail@gmail.com",
            "sender_pass": "sender_gmail_pass",
        }

        with self.assertRaises(AttributeError):
            SharesProfit(self.shares_list, mail_config=mail_config)

    def test_getCurrencyConversionRates1(self):
        """Test that correct conversion rates are scraped and saved - take 1"""
        with patch(
            "src.shares_profit_calc.SharesProfit.get_end_stock_price"
        ) as mock_get_end_stock_price:
            with patch("src.currency_conversion.update_db") as mock_update_db:
                with patch(
                    "src.currency_conversion.get_conversion_rate_db"
                ) as mock_get_conversion_rate_db:
                    mock_get_conversion_rate_db.side_effect = (
                        fake_get_conversion_rate_db
                    )

                    shares_profit = SharesProfit(
                        self.shares_list,
                        mail_config=self.mail_config,
                    )
                    # Test that correct currency is scraped with correct dates
                    mock_update_db.assert_called_once_with(
                        "db-USD.csv", "USD", date(2021, 5, 28)
                    )
                    mock_get_conversion_rate_db.assert_called_once_with(
                        "db-USD.csv", "USD"
                    )
                    self.assertEqual(
                        shares_profit.currency_conversion["db"]["USD"],
                        [("dateUSD", "conv_ratioUSD")],
                    )

                # Remove unused warnings
                _ = mock_get_end_stock_price

    def test_getCurrencyConversionRates2(self):
        """Test that correct conversion rates are scraped and saved - take 2"""
        with patch(
            "src.shares_profit_calc.SharesProfit.get_end_stock_price"
        ) as mock_get_end_stock_price:
            with patch("src.currency_conversion.update_db") as mock_update_db:
                # Remove unused warnings
                _ = mock_get_end_stock_price

                with patch(
                    "src.currency_conversion.get_conversion_rate_db"
                ) as mock_get_conversion_rate_db:
                    mock_get_conversion_rate_db.side_effect = (
                        fake_get_conversion_rate_db
                    )

                    self.shares_list[0]["currency"] = "GBP"
                    self.shares_list[0]["date_buy"] = "2021/04/28"
                    shares_profit = SharesProfit(
                        self.shares_list,
                        mail_config=self.mail_config,
                    )
                    # Test that correct currency is scraped with correct dates
                    mock_update_db.assert_has_calls(
                        [
                            call("db-USD.csv", "USD", date(2021, 5, 28)),
                            call("db-GBP.csv", "GBP", date(2021, 4, 28)),
                        ],
                        any_order=True,
                    )
                    mock_get_conversion_rate_db.assert_has_calls(
                        [call("db-USD.csv", "USD"), call("db-GBP.csv", "GBP")],
                        any_order=True,
                    )
                    self.assertEqual(
                        shares_profit.currency_conversion["db"]["USD"],
                        [("dateUSD", "conv_ratioUSD")],
                    )
                    self.assertEqual(
                        shares_profit.currency_conversion["db"]["GBP"],
                        [("dateGBP", "conv_ratioGBP")],
                    )

    def test_getCurrentPricesForHoldingSharesAndStocks(self):
        """Test that correct `price_end` is set for each share and stock"""
        with patch(
            "src.shares_profit_calc.SharesProfit.get_currency_conversion_rates"
        ) as mock_get_currency_conversion_rates:
            # Remove unused warnings
            _ = mock_get_currency_conversion_rates

            with patch(
                "src.shares_profit_calc.SharesProfit.get_latest_stock_price"
            ) as mock_get_latest_stock_price:
                mock_get_latest_stock_price.side_effect = fake_get_latest_stock_price
                shares_profit = SharesProfit(self.shares_list)
                for share in shares_profit.shares_list:
                    if share["symbol"] == "AAPL":
                        # Share is sold - don't scrape current price
                        self.assertEqual(150, share["price_end"])
                        self.assertEqual(150, shares_profit.price_end[share["symbol"]])
                    elif share["symbol"] == "MSFT":
                        self.assertEqual(200, share["price_end"])
                        self.assertEqual(200, shares_profit.price_end[share["symbol"]])
                    elif share["symbol"] == "GOOGL":
                        self.assertEqual(100, share["price_end"])
                        self.assertEqual(100, shares_profit.price_end[share["symbol"]])

    def test_setCurrenciesForStocks(self):
        """Test that correct `currency` is set for each stock"""
        with patch(
            "src.shares_profit_calc.SharesProfit.get_currency_conversion_rates"
        ) as mock_get_currency_conversion_rates:
            # Remove unused warnings
            _ = mock_get_currency_conversion_rates

            with patch(
                "src.shares_profit_calc.SharesProfit.get_latest_stock_price"
            ) as mock_get_latest_stock_price:
                mock_get_latest_stock_price.side_effect = fake_get_latest_stock_price
                shares_profit = SharesProfit(self.shares_list)
                for share in shares_profit.shares_list:
                    if share["symbol"] == "AAPL":
                        # Share is sold - don't scrape current price
                        self.assertEqual("USD", shares_profit.stock_currency[share["symbol"]])
                    elif share["symbol"] == "MSFT":
                        self.assertEqual("USD", shares_profit.stock_currency[share["symbol"]])
                    elif share["symbol"] == "GOOGL":
                        self.assertEqual("EUR", shares_profit.stock_currency[share["symbol"]])

    def test_dontScrapeCurrentPricesWhenUsingOnlyForSoldShares(self):
        """Test that current prices are not scraped, when script is using ONLY_SOLD for
        `shares_to_use`"""
        with patch(
            "src.shares_profit_calc.SharesProfit.get_currency_conversion_rates"
        ) as mock_get_currency_conversion_rates:
            # Remove unused warnings
            _ = mock_get_currency_conversion_rates

            with patch(
                "src.shares_profit_calc.SharesProfit.get_latest_stock_price"
            ) as mock_get_latest_stock_price:
                shares_profit = SharesProfit(
                    self.shares_list, shares_to_use=SharesToUse.ONLY_SOLD
                )
                mock_get_latest_stock_price.assert_not_called()
                for share in shares_profit.shares_list:
                    if share["symbol"] == "AAPL":
                        # Share is sold - don't scrape current price
                        self.assertEqual(150, share["price_end"])
                        self.assertEqual(150, shares_profit.price_end[share["symbol"]])
                    elif share["symbol"] == "MSFT":
                        # Set to 0, so that other values can be calculated
                        # Note that we don't care about the values calculated later (we just
                        # calculate them because it is easier to test)
                        self.assertEqual(0, share["price_end"])
                    elif share["symbol"] == "GOOGL":
                        # Set to 0, so that other values can be calculated
                        # Note that we don't care about the values calculated later (we just
                        # calculate them because it is easier to test)
                        self.assertEqual(0, share["price_end"])

    def test_getCurrentPricesForHoldingSharesOnlyOncePerStock(self):
        """Check that we don't scrape price for one stock multiple times
        (one price scrape per stock, even if we bought shares multiple times)"""
        with patch(
            "src.shares_profit_calc.SharesProfit.get_currency_conversion_rates"
        ) as mock_get_currency_conversion_rates:
            # Remove unused warnings
            _ = mock_get_currency_conversion_rates

            with patch(
                "src.shares_profit_calc.SharesProfit.get_latest_stock_price"
            ) as mock_get_latest_stock_price:
                _ = SharesProfit(self.shares_list)
                mock_get_latest_stock_price.assert_has_calls(
                    [call("MSFT"), call("GOOGL")], any_order=True
                )
                self.assertEqual(2, mock_get_latest_stock_price.call_count)

    def test_scrapeCurrentPrice(self):
        """Test scraping of current price"""
        with patch(
            "src.shares_profit_calc.SharesProfit.get_currency_conversion_rates"
        ) as mock_get_currency_conversion_rates:
            with patch(
                "src.shares_profit_calc.SharesProfit.get_end_stock_price"
            ) as mock_get_end_stock_price:
                # Remove unused warnings
                _ = mock_get_currency_conversion_rates
                _ = mock_get_end_stock_price

                with patch("src.shares_profit_calc.yfq.Ticker") as mock_yahooquery:
                    shares_profit = SharesProfit(self.shares_list)
                    mock_yahooquery.return_value.price = {
                        "MSFT": {"regularMarketPrice": 200}
                    }
                    self.assertEqual(200, shares_profit.get_latest_stock_price("MSFT"))
                    mock_yahooquery.assert_called_once_with("MSFT")

    def test_getCurrentPricesErrorZeroPrice(self):
        """Test that we raise error in case price is zero"""
        with patch(
            "src.shares_profit_calc.SharesProfit.get_currency_conversion_rates"
        ) as mock_get_currency_conversion_rates:
            # Remove unused warnings
            _ = mock_get_currency_conversion_rates

            with patch(
                "src.shares_profit_calc.SharesProfit.get_latest_stock_price"
            ) as mock_get_latest_stock_price:
                mock_get_latest_stock_price.side_effect = (
                    fake_get_latest_stock_price_zero
                )
                with self.assertRaises(ValueError) as cm:
                    _ = SharesProfit(self.shares_list)

                self.assertEqual(
                    'Stock price is 0 for stock: "MSFT"', str(cm.exception)
                )

    def test_getCurrentPricesErrorScrape(self):
        """Test that we raise and handle error when yfq is not working"""
        with patch(
            "src.shares_profit_calc.SharesProfit.get_currency_conversion_rates"
        ) as mock_get_currency_conversion_rates:
            # Remove unused warnings
            _ = mock_get_currency_conversion_rates

            with patch(
                "src.shares_profit_calc.SharesProfit.get_latest_stock_price"
            ) as mock_get_latest_stock_price:
                mock_get_latest_stock_price.side_effect = (
                    fake_get_latest_stock_price_error
                )
                with self.assertRaises(SystemExit) as cm:
                    f = io.StringIO()
                    with redirect_stderr(f):  # Catch stderr
                        with open(os.devnull, 'w') as devnull:
                            sys.stdout = devnull  # Don't print to stdout
                            _ = SharesProfit(self.shares_list)
                            sys.stdout = sys.__stdout__

                self.assertEqual(
                            "Current price scraping not working: ['GOOGL']\n", f.getvalue()
                )


class TestSharesProfitCalc(unittest.TestCase):
    def setUp(self):
        self.shares_list = [
            {
                "symbol": "AAPL",
                "company": "Apple Inc.",
                "numb_of_shares": 4,
                "currency": "USD",
                "price_buy": 100,
                "price_sell": 150,
                "date_buy": "2021/05/28",
                "date_sell": "2022/05/28",
                "commission_buy": 1,
                "commission_sell": 2,
                "cap_gain_tax": 27.5,
                "price_end": 150,
            },
            {
                "symbol": "MSFT",
                "company": "Microsoft Corporation",
                "numb_of_shares": 2,
                "currency": "GBP",
                "price_buy": 210,
                "price_sell": 0,
                "date_buy": "2021/05/28",
                "date_sell": "",
                "commission_buy": 2,
                "commission_sell": 0,
                "cap_gain_tax": 27.5,
                "price_end": 200,
            },
            {
                "symbol": "GOOGL",
                "company": "Alphabet Inc. Cl A",
                "numb_of_shares": 1,
                "currency": "EUR",
                "price_buy": 90,
                "price_sell": 0,
                "date_buy": "2021/05/28",
                "date_sell": "",
                "commission_buy": 1,
                "commission_sell": 3,
                "cap_gain_tax": 25,
                "price_end": 100,
            },
        ]

        self.mail_config = {
            "sender_mail": "sender@gmail.com",
            "sender_pass": "password",
            "send_to": "recipient@gmail.com",
        }

        with patch(
            "src.shares_profit_calc.SharesProfit.get_currency_conversion_rates"
        ) as mock_get_currency_conversion_rates:
            with patch(
                "src.shares_profit_calc.SharesProfit.get_end_stock_price"
            ) as mock_get_end_stock_price:
                # Remove unused warnings
                _ = mock_get_currency_conversion_rates
                _ = mock_get_end_stock_price

                self.shares_profit = SharesProfit(
                    self.shares_list,
                    mail_config=self.mail_config,
                    shares_to_use=SharesToUse.ALL,
                )

                self.shares_profit.currency_conversion["db"]["USD"] = [("USD-db")]
                self.shares_profit.currency_conversion["db"]["GBP"] = [("GBP-db")]

    def test_calcShareProfitBaseEur(self):
        """Test calculation of share profit - base currency EUR"""
        self.shares_profit.currency = Currency.EUR
        with patch(
            "src.shares_profit_calc.cc.get_conversion_rate"
        ) as mock_get_conversion_rate:
            mock_get_conversion_rate.side_effect = fake_get_conversion_rate1

            self.shares_profit.calc_shares_data(),

            # Sell date specified, positive profit, currency conversion to USD
            # {
            #     "symbol": "AAPL",
            #     "company": "Apple Inc.",
            #     "numb_of_shares": 4,
            #     "currency": "USD",
            #     "price_buy": 100,
            #     "price_sell": 150,
            #     "date_buy": "2021/05/28",   (1 EUR = 2 USD)
            #     "date_sell": "2022/05/28",  (1 EUR = 2.5 USD)
            #     "commission_buy": 1,
            #     "commission_sell": 2,
            #     "cap_gain_tax": 27.5,
            #     "price_end": None,
            # }
            share_profit = self.shares_profit.shares_list[0]
            # In share["currency"]
            self.assertEqual(((1 + 2) / 4) + 100, share_profit["break_even_price"])

            # In script currency
            self.assertAlmostEqual((4 * 100 + 1) / 2, share_profit["investment_initial"])
            self.assertAlmostEqual(
                share_profit["investment_initial"] + 2 / 2.5, share_profit["investment_total"]
            )
            self.assertAlmostEqual(
                4 * (150 / 2.5 - 100 / 2) * 27.5 / 100, share_profit["tax"]
            )
            self.assertAlmostEqual(
                (4 * (150 / 2.5 - 100 / 2)) - (1 / 2) - (2 / 2.5) - share_profit["tax"],
                share_profit["net_profit"],
            )
            self.assertAlmostEqual(
                share_profit["net_profit"] / ((4 * 100 + 1) / 2 + (2 / 2.5)) * 100,
                share_profit["return_on_investment"],
            )
            self.assertAlmostEqual((4 * 150) / 2.5, share_profit["proceeds"])
            self.assertAlmostEqual(
                share_profit["proceeds"]
                - share_profit["tax"]
                - share_profit["commission_sell"] / 2.5,
                share_profit["proceeds_real"],
            )

            # Sell date not specified, negative profit, currency conversion to GBP
            # {
            #     "symbol": "MSFT",
            #     "company": "Microsoft Corporation",
            #     "numb_of_shares": 2,
            #     "currency": "GBP",
            #     "price_buy": 210,
            #     "price_sell": 0,
            #     "date_buy": "2021/05/28",   (1 EUR = 2.1 GBP)
            #     "date_sell": "",            (1 EUR = 4 GBP)
            #     "commission_buy": 2,
            #     "commission_sell": 0,
            #     "cap_gain_tax": 27.5,
            #     "price_end": 200,
            # }
            share_profit = self.shares_profit.shares_list[1]
            # In share["currency"]
            self.assertEqual(((2 + 0) / 2) + 210, share_profit["break_even_price"])

            # In script currency
            self.assertAlmostEqual((2 * 210 + 2) / 2.1, share_profit["investment_initial"])
            self.assertAlmostEqual(
                share_profit["investment_initial"] + 0 / 4, share_profit["investment_total"]
            )
            self.assertEqual(0, share_profit["tax"])  # No tax on a loss
            self.assertAlmostEqual(
                (2 * (200 / 4 - 210 / 2.1)) - (2 / 2.1) - (0 / 4) - share_profit["tax"],
                share_profit["net_profit"],
            )
            self.assertAlmostEqual(
                share_profit["net_profit"] / ((2 * 210 + 2) / 2.1 + (0 / 4)) * 100,
                share_profit["return_on_investment"],
            )
            self.assertAlmostEqual((2 * 200) / 4, share_profit["proceeds"])
            self.assertAlmostEqual(
                share_profit["proceeds"]
                - share_profit["tax"]
                - share_profit["commission_sell"] / 4,
                share_profit["proceeds_real"],
            )

            # Sell date not specified, positive profit, no currency conversion
            # {
            #     "symbol": "GOOGL",
            #     "company": "Alphabet Inc. Cl A",
            #     "numb_of_shares": 1,
            #     "currency": "EUR",
            #     "price_buy": 90,
            #     "price_sell": 0,
            #     "date_buy": "2021/05/28",
            #     "date_sell": "",
            #     "commission_buy": 1,
            #     "commission_sell": 3,
            #     "cap_gain_tax": 25,
            #     "price_end": 100,
            # },
            share_profit = self.shares_profit.shares_list[2]
            # In share["currency"]
            self.assertEqual(((1 + 3) / 1) + 90, share_profit["break_even_price"])

            # In script currency
            self.assertAlmostEqual(1 * 90 + 1, share_profit["investment_initial"])
            self.assertAlmostEqual(
                share_profit["investment_initial"] + 3, share_profit["investment_total"]
            )
            self.assertAlmostEqual(1 * (100 - 90) * 25 / 100, share_profit["tax"])
            self.assertAlmostEqual(
                (1 * (100 - 90)) - 1 - 3 - share_profit["tax"],
                share_profit["net_profit"],
            )
            self.assertAlmostEqual(
                share_profit["net_profit"] / share_profit["investment_total"] * 100,
                share_profit["return_on_investment"],
            )
            self.assertAlmostEqual(1 * 100, share_profit["proceeds"])
            self.assertAlmostEqual(
                1 * 100 - share_profit["tax"] - share_profit["commission_sell"],
                share_profit["proceeds_real"],
            )

    def test_calcShareProfitBaseUSD(self):
        """Test calculation of share profit - base currency USD"""
        self.shares_profit.currency = Currency.USD

        with patch(
            "src.shares_profit_calc.cc.get_conversion_rate"
        ) as mock_get_conversion_rate:
            mock_get_conversion_rate.side_effect = fake_get_conversion_rate2

            self.shares_profit.calc_shares_data(),

            # Sell date specified, positive profit, currency conversion to USD
            # {
            #     "symbol": "AAPL",
            #     "company": "Apple Inc.",
            #     "numb_of_shares": 4,
            #     "currency": "USD",
            #     "price_buy": 100,
            #     "price_sell": 150,
            #     "date_buy": "2021/05/28",
            #     "date_sell": "2022/05/28",
            #     "commission_buy": 1,
            #     "commission_sell": 2,
            #     "cap_gain_tax": 27.5,
            #     "price_end": None,
            # }
            share_profit = self.shares_profit.shares_list[0]
            # In share["currency"]
            self.assertEqual(((1 + 2) / 4) + 100, share_profit["break_even_price"])

            # In script currency
            self.assertAlmostEqual((4 * 100 + 1) / 1, share_profit["investment_initial"])
            self.assertAlmostEqual(
                share_profit["investment_initial"] + 2 / 1, share_profit["investment_total"]
            )
            self.assertAlmostEqual(
                4 * (150 / 1 - 100 / 1) * 27.5 / 100, share_profit["tax"]
            )
            self.assertAlmostEqual(
                (4 * (150 / 1 - 100 / 1)) - (1 / 1) - (2 / 1) - share_profit["tax"],
                share_profit["net_profit"],
            )
            self.assertAlmostEqual(
                share_profit["net_profit"] / ((4 * 100 + 1) / 1 + (2 / 1)) * 100,
                share_profit["return_on_investment"],
            )
            self.assertAlmostEqual((4 * 150) / 1, share_profit["proceeds"])
            self.assertAlmostEqual(
                share_profit["proceeds"]
                - share_profit["tax"]
                - share_profit["commission_sell"] / 1,
                share_profit["proceeds_real"],
            )

            # Sell date not specified, negative profit, currency conversion to GBP
            # {
            #     "symbol": "MSFT",
            #     "company": "Microsoft Corporation",
            #     "numb_of_shares": 2,
            #     "currency": "GBP",
            #     "price_buy": 210,
            #     "price_sell": 0,
            #     "date_buy": "2021/05/28",   (1 EUR = 2.1 GBP = 2 USD)
            #     "date_sell": "",            (1 EUR = 4 GBP = 2.5 USD)
            #     "commission_buy": 2,
            #     "commission_sell": 0,
            #     "cap_gain_tax": 27.5,
            #     "price_end": 200,
            # }
            share_profit = self.shares_profit.shares_list[1]
            # In share["currency"]
            self.assertEqual(((2 + 0) / 2) + 210, share_profit["break_even_price"])

            # In script currency
            self.assertAlmostEqual((2 * 210 + 2) / 2.1 * 2, share_profit["investment_initial"])
            self.assertAlmostEqual(
                share_profit["investment_initial"] + 0 / 4 * 2.5, share_profit["investment_total"]
            )
            self.assertEqual(0, share_profit["tax"])  # No tax on a loss
            self.assertAlmostEqual(
                (2 * (200 / 4 * 2.5 - 210 / 2.1 * 2))
                - (2 / 2.1 * 2)
                - (0 / 4 * 2.5)
                - share_profit["tax"],
                share_profit["net_profit"],
            )
            self.assertAlmostEqual(
                share_profit["net_profit"]
                / ((2 * 210 + 2) / 2.1 * 2 + (0 / 4 * 2.5))
                * 100,
                share_profit["return_on_investment"],
            )
            self.assertAlmostEqual((2 * 200) / 4 * 2.5, share_profit["proceeds"])
            self.assertAlmostEqual(
                share_profit["proceeds"]
                - share_profit["tax"]
                - share_profit["commission_sell"] / 4 * 2.5,
                share_profit["proceeds_real"],
            )

            # Sell date not specified, positive profit, no currency conversion
            # {
            #     "symbol": "GOOGL",
            #     "company": "Alphabet Inc. Cl A",
            #     "numb_of_shares": 1,
            #     "currency": "EUR",
            #     "price_buy": 90,
            #     "price_sell": 0,
            #     "date_buy": "2021/05/28",    (1 EUR = 2 USD)
            #     "date_sell": "",             (1 EUR = 2.5 USD)
            #     "commission_buy": 1,
            #     "commission_sell": 3,
            #     "cap_gain_tax": 25,
            #     "price_end": 100,
            # },
            share_profit = self.shares_profit.shares_list[2]
            # In share["currency"]
            self.assertEqual(((1 + 3) / 1) + 90, share_profit["break_even_price"])

            # In script currency
            self.assertAlmostEqual((1 * 90 + 1) * 2, share_profit["investment_initial"])
            self.assertAlmostEqual(
                share_profit["investment_initial"] + 3 * 2.5, share_profit["investment_total"]
            )
            self.assertAlmostEqual(
                1 * (100 * 2.5 - 90 * 2) * 25 / 100, share_profit["tax"]
            )
            self.assertAlmostEqual(
                (1 * (100 * 2.5 - 90 * 2)) - 1 * 2 - 3 * 2.5 - share_profit["tax"],
                share_profit["net_profit"],
            )
            self.assertAlmostEqual(
                share_profit["net_profit"] / ((1 * 90 + 1) * 2 + (3 * 2.5)) * 100,
                share_profit["return_on_investment"],
            )
            self.assertAlmostEqual(1 * 100 * 2.5, share_profit["proceeds"])
            self.assertAlmostEqual(
                1 * 100 * 2.5
                - share_profit["tax"]
                - share_profit["commission_sell"] * 2.5,
                share_profit["proceeds_real"],
            )

    def test_calcTotalProceeds(self):
        """Test get total proceeds"""
        self.shares_profit.shares_list = [
            {
                "symbol": "AAPL",
                "proceeds": (3 * 150),
                "date_sell": "2022/05/28",
            },
            {
                "symbol": "MSFT",
                "proceeds": (2 * 200),
                "date_sell": "",
            },
            {
                "symbol": "GOOGL",
                "proceeds": 100,
                "date_sell": "",
            },
        ]

        self.shares_profit.shares_to_use = SharesToUse.ALL
        self.assertEqual(
            3 * 150 + 2 * 200 + 100,
            self.shares_profit.get_total_proceeds(),
        )
        self.shares_profit.shares_to_use = SharesToUse.ONLY_HOLDING
        self.assertEqual(
            2 * 200 + 100,
            self.shares_profit.get_total_proceeds(),
        )
        self.shares_profit.shares_to_use = SharesToUse.ONLY_SOLD
        self.assertEqual(
            3 * 150,
            self.shares_profit.get_total_proceeds(),
        )

    def test_calcTotalRealProceeds(self):
        """Test total real proceeds"""
        self.shares_profit.shares_list = [
            {
                "symbol": "AAPL",
                "date_sell": "2022/05/28",
                "proceeds_real": (3 * 150),
            },
            {
                "symbol": "MSFT",
                "date_sell": "",
                "proceeds_real": (2 * 200),
            },
            {
                "symbol": "GOOGL",
                "date_sell": "",
                "proceeds_real": 100,
            },
        ]
        self.shares_profit.shares_to_use = SharesToUse.ALL
        self.assertEqual(
            3 * 150 + 2 * 200 + 100,
            self.shares_profit.get_total_real_proceeds(),
        )
        self.shares_profit.shares_to_use = SharesToUse.ONLY_HOLDING
        self.assertEqual(
            2 * 200 + 100,
            self.shares_profit.get_total_real_proceeds(),
        )
        self.shares_profit.shares_to_use = SharesToUse.ONLY_SOLD
        self.assertEqual(
            3 * 150,
            self.shares_profit.get_total_real_proceeds(),
        )

    def test_getSharePercPerTotalProceeds(self):
        """Test share percentages against total proceeds"""
        self.shares_profit.shares_list = [
            {
                "symbol": "AAPL",
                "date_sell": "2022/05/28",
                "proceeds": 50,
            },
            {
                "symbol": "MSFT",
                "date_sell": "",
                "proceeds": 30,
            },
            {
                "symbol": "GOOGL",
                "date_sell": "",
                "proceeds": 20,
            },
        ]
        self.shares_profit.total_proceeds = 100
        self.shares_profit.shares_to_use = SharesToUse.ALL
        self.shares_profit.set_share_perc_per_total_proceeds(
            self.shares_profit.shares_list[0]
        )
        self.assertEqual(
            50, self.shares_profit.shares_list[0]["share_perc_per_total_proceeds"]
        )
        self.shares_profit.set_share_perc_per_total_proceeds(
            self.shares_profit.shares_list[1]
        )
        self.assertEqual(
            30, self.shares_profit.shares_list[1]["share_perc_per_total_proceeds"]
        )
        self.shares_profit.set_share_perc_per_total_proceeds(
            self.shares_profit.shares_list[2]
        )
        self.assertEqual(
            20, self.shares_profit.shares_list[2]["share_perc_per_total_proceeds"]
        )

        self.shares_profit.total_proceeds = 50
        self.shares_profit.shares_to_use = SharesToUse.ONLY_HOLDING
        self.shares_profit.set_share_perc_per_total_proceeds(
            self.shares_profit.shares_list[0]
        )
        self.assertEqual(
            0, self.shares_profit.shares_list[0]["share_perc_per_total_proceeds"]
        )
        self.shares_profit.set_share_perc_per_total_proceeds(
            self.shares_profit.shares_list[1]
        )
        self.assertEqual(
            60, self.shares_profit.shares_list[1]["share_perc_per_total_proceeds"]
        )
        self.shares_profit.set_share_perc_per_total_proceeds(
            self.shares_profit.shares_list[2]
        )
        self.assertEqual(
            40, self.shares_profit.shares_list[2]["share_perc_per_total_proceeds"]
        )

        self.shares_profit.total_proceeds = 50
        self.shares_profit.shares_to_use = SharesToUse.ONLY_SOLD
        self.shares_profit.set_share_perc_per_total_proceeds(
            self.shares_profit.shares_list[0]
        )
        self.assertEqual(
            100, self.shares_profit.shares_list[0]["share_perc_per_total_proceeds"]
        )
        self.shares_profit.set_share_perc_per_total_proceeds(
            self.shares_profit.shares_list[1]
        )
        self.assertEqual(
            0, self.shares_profit.shares_list[1]["share_perc_per_total_proceeds"]
        )
        self.shares_profit.set_share_perc_per_total_proceeds(
            self.shares_profit.shares_list[2]
        )
        self.assertEqual(
            0, self.shares_profit.shares_list[2]["share_perc_per_total_proceeds"]
        )

    def test_getSharePercPerTotalRealProceeds(self):
        """Test share percentages against total real proceeds"""
        self.shares_profit.shares_list = [
            {
                "symbol": "AAPL",
                "date_sell": "2022/05/28",
                "proceeds_real": 50,
            },
            {
                "symbol": "MSFT",
                "date_sell": "",
                "proceeds_real": 30,
            },
            {
                "symbol": "GOOGL",
                "date_sell": "",
                "proceeds_real": 20,
            },
        ]
        self.shares_profit.total_real_proceeds = 100
        self.shares_profit.shares_to_use = SharesToUse.ALL
        self.shares_profit.set_share_perc_per_total_real_proceeds(
            self.shares_profit.shares_list[0]
        )
        self.assertEqual(
            50, self.shares_profit.shares_list[0]["share_perc_per_total_real_proceeds"]
        )
        self.shares_profit.set_share_perc_per_total_real_proceeds(
            self.shares_profit.shares_list[1]
        )
        self.assertEqual(
            30, self.shares_profit.shares_list[1]["share_perc_per_total_real_proceeds"]
        )
        self.shares_profit.set_share_perc_per_total_real_proceeds(
            self.shares_profit.shares_list[2]
        )
        self.assertEqual(
            20, self.shares_profit.shares_list[2]["share_perc_per_total_real_proceeds"]
        )

        self.shares_profit.total_real_proceeds = 50
        self.shares_profit.shares_to_use = SharesToUse.ONLY_HOLDING
        self.shares_profit.set_share_perc_per_total_real_proceeds(
            self.shares_profit.shares_list[0]
        )
        self.assertEqual(
            0, self.shares_profit.shares_list[0]["share_perc_per_total_real_proceeds"]
        )
        self.shares_profit.set_share_perc_per_total_real_proceeds(
            self.shares_profit.shares_list[1]
        )
        self.assertEqual(
            60, self.shares_profit.shares_list[1]["share_perc_per_total_real_proceeds"]
        )
        self.shares_profit.set_share_perc_per_total_real_proceeds(
            self.shares_profit.shares_list[2]
        )
        self.assertEqual(
            40, self.shares_profit.shares_list[2]["share_perc_per_total_real_proceeds"]
        )

        self.shares_profit.total_real_proceeds = 50
        self.shares_profit.shares_to_use = SharesToUse.ONLY_SOLD
        self.shares_profit.set_share_perc_per_total_real_proceeds(
            self.shares_profit.shares_list[0]
        )
        self.assertEqual(
            100, self.shares_profit.shares_list[0]["share_perc_per_total_real_proceeds"]
        )
        self.shares_profit.set_share_perc_per_total_real_proceeds(
            self.shares_profit.shares_list[1]
        )
        self.assertEqual(
            0, self.shares_profit.shares_list[1]["share_perc_per_total_real_proceeds"]
        )
        self.shares_profit.set_share_perc_per_total_real_proceeds(
            self.shares_profit.shares_list[2]
        )
        self.assertEqual(
            0, self.shares_profit.shares_list[2]["share_perc_per_total_real_proceeds"]
        )

    def test_getTotalProceedsPerStock(self):
        """Test total stock proceeds"""
        self.shares_profit.shares_list = [
            {
                "symbol": "AAPL",
                "date_sell": "2022/05/28",
                "proceeds": 150,
            },
            {
                "symbol": "MSFT",
                "date_sell": "",
                "proceeds": 200,
            },
            {
                "symbol": "GOOGL",
                "date_sell": "",
                "proceeds": 100,
            },
            {
                "symbol": "GOOGL",
                "date_sell": "",
                "proceeds": 100,
            },
        ]
        self.shares_profit.shares_to_use = SharesToUse.ALL
        self.assertEqual(
            {"AAPL": 150, "MSFT": 200, "GOOGL": 200},
            self.shares_profit.get_total_proceeds_per_stock(),
        )

        self.shares_profit.shares_to_use = SharesToUse.ONLY_HOLDING
        self.assertEqual(
            {"MSFT": 200, "GOOGL": 200},
            self.shares_profit.get_total_proceeds_per_stock(),
        )

        self.shares_profit.shares_to_use = SharesToUse.ONLY_SOLD
        self.shares_profit.currency = Currency.EUR
        self.assertEqual(
            {"AAPL": 150},
            self.shares_profit.get_total_proceeds_per_stock(),
        )

    def test_getTotalRealProceedsPerStock(self):
        """Test total real stock proceeds"""
        self.shares_profit.shares_list = [
            {
                "symbol": "AAPL",
                "date_sell": "2022/05/28",
                "proceeds_real": 150,
            },
            {
                "symbol": "MSFT",
                "date_sell": "",
                "proceeds_real": 200,
            },
            {
                "symbol": "GOOGL",
                "date_sell": "",
                "proceeds_real": 100,
            },
            {
                "symbol": "GOOGL",
                "date_sell": "",
                "proceeds_real": 100,
            },
        ]
        self.shares_profit.shares_to_use = SharesToUse.ALL
        self.assertEqual(
            {"AAPL": 150, "MSFT": 200, "GOOGL": 200},
            self.shares_profit.get_total_real_proceeds_per_stock(),
        )

        self.shares_profit.shares_to_use = SharesToUse.ONLY_HOLDING
        self.assertEqual(
            {"MSFT": 200, "GOOGL": 200},
            self.shares_profit.get_total_real_proceeds_per_stock(),
        )

        self.shares_profit.shares_to_use = SharesToUse.ONLY_SOLD
        self.assertEqual(
            {"AAPL": 150},
            self.shares_profit.get_total_real_proceeds_per_stock(),
        )

    def test_getStockPercPerTotalProceeds(self):
        """
        Test stock percentages against total proceeds
        Note that `self.total_proceeds` and `self.total_proceeds_per_stock` are already calculted
        for desired `self.shares_to_use`
        """
        self.shares_profit.total_proceeds = 1000
        self.shares_profit.total_proceeds_per_stock = {
            "AAPL": 200,
            "MSFT": 700,
            "GOOGL": 300,
        }
        self.assertEqual(
            {"AAPL": 20, "MSFT": 70, "GOOGL": 30},
            self.shares_profit.get_stock_perc_per_total_proceeds(),
        )

    def test_getStockPercPerTotalRealProceeds(self):
        """
        Test stock percentages against total real proceeds
        Note that `self.total_proceeds` and `self.total_proceeds_per_stock` are already calculted
        for desired `self.shares_to_use`
        """
        self.shares_profit.total_real_proceeds = 200
        self.shares_profit.total_real_proceeds_per_stock = {
            "AAPL": 40,
            "MSFT": 60,
            "GOOGL": 100,
        }
        self.assertEqual(
            {"AAPL": 20, "MSFT": 30, "GOOGL": 50},
            self.shares_profit.get_stock_perc_per_total_real_proceeds(),
        )

    def test_addTotalStockProceedsToSharesList(self):
        """Test adding of total stock proceeds to shares list"""
        self.shares_profit.shares_list = [
            {
                "symbol": "AAPL",
                "date_sell": "2022/05/28",
            },
            {
                "symbol": "MSFT",
                "date_sell": "",
            },
            {
                "symbol": "GOOGL",
                "date_sell": "",
            },
            {
                "symbol": "GOOGL",
                "date_sell": "",
            },
            {
                "symbol": "GOOGL",
                "date_sell": "2022/05/28",
            },
        ]
        self.shares_profit.total_proceeds_per_stock = {
            "AAPL": 100,
            "MSFT": 200,
            "GOOGL": 300,
        }
        self.shares_profit.shares_to_use = SharesToUse.ALL
        self.assertEqual(
            [
                {
                    "symbol": "AAPL",
                    "date_sell": "2022/05/28",
                    "stock_proceeds": 100,
                },
                {
                    "symbol": "MSFT",
                    "date_sell": "",
                    "stock_proceeds": 200,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_proceeds": 300,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_proceeds": 300,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "2022/05/28",
                    "stock_proceeds": 300,
                },
            ],
            self.shares_profit.add_total_stock_proceeds_to_shares_list(),
        )

        self.shares_profit.shares_to_use = SharesToUse.ONLY_HOLDING
        self.assertEqual(
            [
                {
                    "symbol": "AAPL",
                    "date_sell": "2022/05/28",
                    "stock_proceeds": 0,
                },
                {
                    "symbol": "MSFT",
                    "date_sell": "",
                    "stock_proceeds": 200,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_proceeds": 300,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_proceeds": 300,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "2022/05/28",
                    "stock_proceeds": 0,
                },
            ],
            self.shares_profit.add_total_stock_proceeds_to_shares_list(),
        )

        self.shares_profit.shares_to_use = SharesToUse.ONLY_SOLD
        self.assertEqual(
            [
                {
                    "symbol": "AAPL",
                    "date_sell": "2022/05/28",
                    "stock_proceeds": 100,
                },
                {
                    "symbol": "MSFT",
                    "date_sell": "",
                    "stock_proceeds": 0,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_proceeds": 0,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_proceeds": 0,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "2022/05/28",
                    "stock_proceeds": 300,
                },
            ],
            self.shares_profit.add_total_stock_proceeds_to_shares_list(),
        )

    def test_addTotalRealStockProceedsToSharesList(self):
        """Test adding of total real stock proceeds to shares list"""
        self.shares_profit.shares_list = [
            {
                "symbol": "AAPL",
                "date_sell": "2022/05/28",
            },
            {
                "symbol": "MSFT",
                "date_sell": "",
            },
            {
                "symbol": "GOOGL",
                "date_sell": "",
            },
            {
                "symbol": "GOOGL",
                "date_sell": "",
            },
            {
                "symbol": "GOOGL",
                "date_sell": "2022/05/28",
            },
        ]
        self.shares_profit.total_real_proceeds_per_stock = {
            "AAPL": 10,
            "MSFT": 20,
            "GOOGL": 30,
        }
        self.shares_profit.shares_to_use = SharesToUse.ALL
        self.assertEqual(
            [
                {
                    "symbol": "AAPL",
                    "date_sell": "2022/05/28",
                    "stock_proceeds_real": 10,
                },
                {
                    "symbol": "MSFT",
                    "date_sell": "",
                    "stock_proceeds_real": 20,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_proceeds_real": 30,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_proceeds_real": 30,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "2022/05/28",
                    "stock_proceeds_real": 30,
                },
            ],
            self.shares_profit.add_total_real_stock_proceeds_to_shares_list(),
        )

        self.shares_profit.shares_to_use = SharesToUse.ONLY_HOLDING
        self.assertEqual(
            [
                {
                    "symbol": "AAPL",
                    "date_sell": "2022/05/28",
                    "stock_proceeds_real": 0,
                },
                {
                    "symbol": "MSFT",
                    "date_sell": "",
                    "stock_proceeds_real": 20,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_proceeds_real": 30,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_proceeds_real": 30,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "2022/05/28",
                    "stock_proceeds_real": 0,
                },
            ],
            self.shares_profit.add_total_real_stock_proceeds_to_shares_list(),
        )

        self.shares_profit.shares_to_use = SharesToUse.ONLY_SOLD
        self.assertEqual(
            [
                {
                    "symbol": "AAPL",
                    "date_sell": "2022/05/28",
                    "stock_proceeds_real": 10,
                },
                {
                    "symbol": "MSFT",
                    "date_sell": "",
                    "stock_proceeds_real": 0,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_proceeds_real": 0,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_proceeds_real": 0,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "2022/05/28",
                    "stock_proceeds_real": 30,
                },
            ],
            self.shares_profit.add_total_real_stock_proceeds_to_shares_list(),
        )

    def test_addStockPercPerTotalProceedsToSharesList(self):
        """
        Test adding of stock percentages per total proceeds to shares list
        Note that `self.total_proceeds` and `self.total_proceeds_per_stock` are already calculted
        for desired `self.shares_to_use`
        """
        self.shares_profit.shares_list = [
            {
                "symbol": "AAPL",
                "date_sell": "2022/05/28",
            },
            {
                "symbol": "MSFT",
                "date_sell": "",
            },
            {
                "symbol": "GOOGL",
                "date_sell": "",
            },
            {
                "symbol": "GOOGL",
                "date_sell": "",
            },
            {
                "symbol": "GOOGL",
                "date_sell": "2022/05/28",
            },
        ]
        self.shares_profit.total_proceeds = 100
        self.shares_profit.total_proceeds_per_stock = {
            "AAPL": 10,
            "MSFT": 20,
            "GOOGL": 70,
        }
        self.shares_profit.shares_to_use = SharesToUse.ALL
        self.assertEqual(
            [
                {
                    "symbol": "AAPL",
                    "date_sell": "2022/05/28",
                    "stock_perc_per_total_proceeds": 10,
                },
                {
                    "symbol": "MSFT",
                    "date_sell": "",
                    "stock_perc_per_total_proceeds": 20,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_perc_per_total_proceeds": 70,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_perc_per_total_proceeds": 70,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "2022/05/28",
                    "stock_perc_per_total_proceeds": 70,
                },
            ],
            self.shares_profit.add_stock_perc_per_total_proceeds_to_shares_list(),
        )

        self.shares_profit.total_proceeds = 90
        self.shares_profit.shares_to_use = SharesToUse.ONLY_HOLDING
        self.assertEqual(
            [
                {
                    "symbol": "AAPL",
                    "date_sell": "2022/05/28",
                    "stock_perc_per_total_proceeds": 0,
                },
                {
                    "symbol": "MSFT",
                    "date_sell": "",
                    "stock_perc_per_total_proceeds": 20 / 90 * 100,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_perc_per_total_proceeds": 70 / 90 * 100,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_perc_per_total_proceeds": 70 / 90 * 100,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "2022/05/28",
                    "stock_perc_per_total_proceeds": 0,
                },
            ],
            self.shares_profit.add_stock_perc_per_total_proceeds_to_shares_list(),
        )

        self.shares_profit.total_proceeds = 10 + 70
        self.shares_profit.shares_to_use = SharesToUse.ONLY_SOLD
        self.assertEqual(
            [
                {
                    "symbol": "AAPL",
                    "date_sell": "2022/05/28",
                    "stock_perc_per_total_proceeds": 10 / 80 * 100,
                },
                {
                    "symbol": "MSFT",
                    "date_sell": "",
                    "stock_perc_per_total_proceeds": 0,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_perc_per_total_proceeds": 0,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_perc_per_total_proceeds": 0,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "2022/05/28",
                    "stock_perc_per_total_proceeds": 70 / 80 * 100,
                },
            ],
            self.shares_profit.add_stock_perc_per_total_proceeds_to_shares_list(),
        )

    def test_addStockPercPerTotalRealProceedsToSharesList(self):
        """
        Test adding of stock percentages per real total proceeds to shares list
        Note that `self.total_proceeds` and `self.total_proceeds_per_stock` are already calculted
        for desired `self.shares_to_use`
        """
        self.shares_profit.shares_list = [
            {
                "symbol": "AAPL",
                "date_sell": "2022/05/28",
            },
            {
                "symbol": "MSFT",
                "date_sell": "",
            },
            {
                "symbol": "GOOGL",
                "date_sell": "",
            },
            {
                "symbol": "GOOGL",
                "date_sell": "",
            },
            {
                "symbol": "GOOGL",
                "date_sell": "2022/05/28",
            },
        ]
        self.shares_profit.total_real_proceeds = 100
        self.shares_profit.total_real_proceeds_per_stock = {
            "AAPL": 10,
            "MSFT": 20,
            "GOOGL": 70,
        }
        self.shares_profit.shares_to_use = SharesToUse.ALL
        self.assertEqual(
            [
                {
                    "symbol": "AAPL",
                    "date_sell": "2022/05/28",
                    "stock_perc_per_total_real_proceeds": 10,
                },
                {
                    "symbol": "MSFT",
                    "date_sell": "",
                    "stock_perc_per_total_real_proceeds": 20,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_perc_per_total_real_proceeds": 70,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_perc_per_total_real_proceeds": 70,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "2022/05/28",
                    "stock_perc_per_total_real_proceeds": 70,
                },
            ],
            self.shares_profit.add_stock_perc_per_total_real_proceeds_to_shares_list(),
        )

        self.shares_profit.total_real_proceeds = 90
        self.shares_profit.shares_to_use = SharesToUse.ONLY_HOLDING
        self.assertEqual(
            [
                {
                    "symbol": "AAPL",
                    "date_sell": "2022/05/28",
                    "stock_perc_per_total_real_proceeds": 0,
                },
                {
                    "symbol": "MSFT",
                    "date_sell": "",
                    "stock_perc_per_total_real_proceeds": 20 / 90 * 100,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_perc_per_total_real_proceeds": 70 / 90 * 100,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_perc_per_total_real_proceeds": 70 / 90 * 100,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "2022/05/28",
                    "stock_perc_per_total_real_proceeds": 0,
                },
            ],
            self.shares_profit.add_stock_perc_per_total_real_proceeds_to_shares_list(),
        )

        self.shares_profit.total_real_proceeds = 10 + 70
        self.shares_profit.shares_to_use = SharesToUse.ONLY_SOLD
        self.assertEqual(
            [
                {
                    "symbol": "AAPL",
                    "date_sell": "2022/05/28",
                    "stock_perc_per_total_real_proceeds": 10 / 80 * 100,
                },
                {
                    "symbol": "MSFT",
                    "date_sell": "",
                    "stock_perc_per_total_real_proceeds": 0,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_perc_per_total_real_proceeds": 0,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "",
                    "stock_perc_per_total_real_proceeds": 0,
                },
                {
                    "symbol": "GOOGL",
                    "date_sell": "2022/05/28",
                    "stock_perc_per_total_real_proceeds": 70 / 80 * 100,
                },
            ],
            self.shares_profit.add_stock_perc_per_total_real_proceeds_to_shares_list(),
        )

    def test_getStockNetProfit(self):
        """Test net profit for each individual stock"""
        self.shares_profit.shares_list = [
            {
                "symbol": "AAPL",
                "currency": "USD",
                "date_sell": "2022/05/28",
                "net_profit": 50,
            },
            {
                "symbol": "MSFT",
                "currency": "USD",
                "date_sell": "",
                "net_profit": -10,
            },
            {
                "symbol": "GOOGL",
                "currency": "EUR",
                "date_sell": "",
                "net_profit": -10,
            },
            {
                "symbol": "GOOGL",
                "currency": "EUR",
                "date_sell": "",
                "net_profit": 30,
            },
        ]
        self.shares_profit.shares_to_use = SharesToUse.ALL
        stock_net_profit = self.shares_profit.get_stock_net_profit()
        self.assertEqual(50, stock_net_profit["AAPL"])
        self.assertEqual(-10, stock_net_profit["MSFT"])
        self.assertEqual(-10 + 30, stock_net_profit["GOOGL"])

        self.shares_profit.shares_to_use = SharesToUse.ONLY_HOLDING
        self.assertEqual(
            {"MSFT": -10, "GOOGL": -10 + 30},
            self.shares_profit.get_stock_net_profit(),
        )

        self.shares_profit.shares_to_use = SharesToUse.ONLY_SOLD
        self.assertEqual(
            {"AAPL": 50},
            self.shares_profit.get_stock_net_profit(),
        )

    def test_getTotalNetProfit(self):
        """Test total net profit for all stocks"""
        self.shares_profit.stock_net_profit = {"AAPL": 10, "MSFT": 20, "GOOGL": -5}

        self.assertEqual(
            25,
            self.shares_profit.get_total_net_profit(),
        )

    def test_getStockInvestment(self):
        """Test investment total for each individual stock"""
        self.shares_profit.shares_list = [
            {
                "symbol": "AAPL",
                "currency": "USD",
                "date_sell": "2022/05/28",
                "investment_total": 50,
            },
            {
                "symbol": "MSFT",
                "currency": "USD",
                "date_sell": "",
                "investment_total": 100,
            },
            {
                "symbol": "GOOGL",
                "currency": "EUR",
                "date_sell": "",
                "investment_total": 100,
            },
            {
                "symbol": "GOOGL",
                "currency": "EUR",
                "date_sell": "",
                "investment_total": 100,
            },
        ]
        self.shares_profit.shares_to_use = SharesToUse.ALL
        stock_investment_total = self.shares_profit.get_stock_investment()
        self.assertEqual(50, stock_investment_total["AAPL"])
        self.assertEqual(100, stock_investment_total["MSFT"])
        self.assertEqual(100 + 100, stock_investment_total["GOOGL"])

        self.shares_profit.shares_to_use = SharesToUse.ONLY_HOLDING
        self.assertEqual(
            {"MSFT": 100, "GOOGL": 100 + 100},
            self.shares_profit.get_stock_investment(),
        )

        self.shares_profit.shares_to_use = SharesToUse.ONLY_SOLD
        self.assertEqual(
            {"AAPL": 50},
            self.shares_profit.get_stock_investment(),
        )

    def test_getStockReturnOnInvestment(self):
        """Test return on investment for each individual stock"""
        self.shares_profit.stock_net_profit = {"AAPL": 10, "MSFT": 20, "GOOGL": -30}
        self.shares_profit.stock_investment = {"AAPL": 100, "MSFT": 200, "GOOGL": 100}
        self.assertEqual(
            {"AAPL": 10, "MSFT": 10, "GOOGL": -30},
            self.shares_profit.get_stock_return_on_investment(),
        )

    def test_getTotalRetunOnInvestment(self):
        """Test total return on investment for all stocks"""
        self.shares_profit.stock_investment = {"AAPL": 10, "MSFT": 20, "GOOGL": 50}
        self.assertEqual(
            80,
            self.shares_profit.get_total_investment(),
        )

    def test_getTotalReturnOnInvestment(self):
        """Test total investment for all stocks"""
        self.shares_profit.total_net_profit = 100
        self.shares_profit.total_investment = 200
        self.assertEqual(
            50,
            self.shares_profit.get_total_return_on_investment(),
        )


class TestSharesProfitCalcCalledFns(unittest.TestCase):
    def setUp(self):
        self.shares_list = [
            {
                "symbol": "AAPL",
                "company": "Apple Inc.",
                "numb_of_shares": 3,
                "currency": "USD",
                "price_buy": 100,
                "price_sell": 150,
                "date_buy": "2021/05/28",
                "date_sell": "2022/05/28",
                "commission_buy": 0,
                "commission_sell": 1,
                "cap_gain_tax": 20,
                "price_end": 150,
            },
            {
                "symbol": "MSFT",
                "company": "Microsoft Corporation",
                "numb_of_shares": 2,
                "currency": "USD",
                "price_buy": 210,
                "price_sell": 0,
                "date_buy": "2021/05/28",
                "date_sell": "",
                "commission_buy": 2,
                "commission_sell": 0,
                "cap_gain_tax": 27.5,
                "price_end": 200,
            },
            {
                "symbol": "GOOGL",
                "company": "Alphabet Inc. Cl A",
                "numb_of_shares": 1,
                "currency": "EUR",
                "price_buy": 90,
                "price_sell": 0,
                "date_buy": "2021/05/28",
                "date_sell": "",
                "commission_buy": 1,
                "commission_sell": 3,
                "cap_gain_tax": 10,
                "price_end": 100,
            },
        ]

        self.mail_config = {
            "sender_mail": "sender@gmail.com",
            "sender_pass": "password",
            "send_to": "recipient@gmail.com",
        }

        with patch(
            "src.shares_profit_calc.SharesProfit.get_currency_conversion_rates"
        ) as mock_get_currency_conversion_rates:
            with patch(
                "src.shares_profit_calc.SharesProfit.get_end_stock_price"
            ) as mock_get_end_stock_price:
                _ = mock_get_currency_conversion_rates
                _ = mock_get_end_stock_price
                self.shares_profit = SharesProfit(
                    self.shares_list,
                    mail_config=self.mail_config,
                    currency=Currency.EUR,
                    shares_to_use=SharesToUse.ALL,
                )

        self.tax = {}
        self.tax["aapl"] = (3 * (150 - 100)) * 20 / 100
        self.tax["msft"] = 0
        self.tax["googl"] = (100 - 90) * 10 / 100
        self.investment_total = {}
        self.investment_total["aapl"] = (3 * 100) + 0 + 1
        self.investment_total["msft"] = (2 * 210) + 2 + 0
        self.investment_total["googl"] = (1 * 90) + 1 + 3
        self.net_profit = {}
        self.net_profit["aapl"] = (3 * (150 - 100)) - 0 - 1 - self.tax["aapl"]
        self.net_profit["msft"] = (2 * (200 - 210)) - 2 - 0 - self.tax["msft"]
        self.net_profit["googl"] = (1 * (100 - 90)) - 1 - 3 - self.tax["googl"]


if __name__ == "__main__":
    unittest.main()
