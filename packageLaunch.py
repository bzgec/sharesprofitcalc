#!/usr/bin/env python
"""
"""

# Standard libraries
import os
import sys
import time

# 3rd party packages
import typer
from rich import print

# Local source
from src.helperLib.import_config import get_json
from src.shares_profit_calc import Currency, CurrencySymbols, SharesProfit, SharesToUse
from src.shares_profit_print import Mail, Term

SHARES_LIST_FILE_EXAMPLE = "sharesListExample.json"
SHARES_LIST_FILE_DFLT = "config/shares_list.json"
MAIL_CONFIG_FILE = "config/mail_sender.json"


def main(
    term_short: bool = typer.Option(
        False,
        "--short",
        "-s",
        help="Print short stocks profit information to terminal.",
    ),
    term_detailed: bool = typer.Option(
        False,
        "--detailed",
        "-d",
        help="Print detailed shares profit information to terminal.",
    ),
    mail_short: bool = typer.Option(
        False, "--mail-short", help="Send short stocks profit information via mail."
    ),
    mail_detailed: bool = typer.Option(
        False,
        "--mail-detailed",
        help="Send detailed shares profit information via mail.",
    ),
    use_only_holding: bool = typer.Option(
        False, "--only-holding", help="Use shares currently holding in calculation."
    ),
    use_only_sold: bool = typer.Option(
        False,
        "--only-sold",
        help="Use only sold shares in calculations.",
    ),
    print_shares_list_example: bool = typer.Option(
        False,
        "--example-file",
        help="Print example shares list file to terminal",
    ),
    shares_list_file: str = typer.Option(
        SHARES_LIST_FILE_DFLT,
        "--shares-file",
        "-f",
        help="""Shares list configuration file.\n
             If this file is not specified the script takes shares list
             from './config/sharesList.json'. For example take a look at
             'sharesListExample.json' or pass '--example-file' to script to
             print it to terminal.\n
             Example:\n
               - `make SCRIPT_PARAM='-sd --file mySharesList.json`""",
    ),
    currency_str: str = typer.Option(
        "EUR",
        "--currency",
        "-c",
        help="""Currency used when summing all stocks.\n
             Options are:\n
             - USD\n
             - EUR""",
    ),
    mail_config_file: str = typer.Option(
        MAIL_CONFIG_FILE,
        "--mail-config-file",
        help="""Mail configuration file.\n
             Example:\n
             "mail_config.json":\n
             -------------------------------------------------\n
             {\n
                 "sender_mail": "sender.gmail@gmail.com",\n
                 "sender_pass": "sender_gmail_pass",\n
                 "send_to": "recipient.mail@gmail.com"\n
             }""",
    ),
):
    shares_list = None
    mail_config = {}
    shares_to_use = SharesToUse.ALL
    currency = Currency.EUR
    currency_symbol = CurrencySymbols[currency.value]
    print_str = ""
    mail_str = ""
    term = Term()
    mail = Mail()
    script_dir = os.path.realpath(os.path.dirname(__file__))

    print("Script started...")

    # If configuration file is NOT passed, use the default location with absolute path
    if shares_list_file == SHARES_LIST_FILE_DFLT:
        shares_list_file = f"{script_dir}/{shares_list_file}"
    if mail_config_file == MAIL_CONFIG_FILE:
        mail_config_file = f"{script_dir}/{mail_config_file}"

    if use_only_holding is True:
        shares_to_use = SharesToUse.ONLY_HOLDING
    elif use_only_sold is True:
        shares_to_use = SharesToUse.ONLY_SOLD

    if currency_str == "EUR":
        currency = Currency.EUR
    elif currency_str == "USD":
        currency = Currency.USD

    currency_symbol = CurrencySymbols[currency.value]

    # Print example shares list file
    if print_shares_list_example is True:
        with open(
            f"{script_dir}/{SHARES_LIST_FILE_EXAMPLE}", "rt", encoding="UTF-8"
        ) as file:
            print(file.read())
        sys.exit(0)

    # Get shares list
    shares_list = get_json(shares_list_file)

    if mail_short is True or mail_detailed is True:
        # Get mail configuration
        mail_config = get_json(mail_config_file)

    # Start calculation time measurement
    time_start = time.perf_counter()

    # Initialize shares profit
    shares_profit = SharesProfit(
        shares_list,
        mail_config=mail_config,
        currency=currency,
        shares_to_use=shares_to_use,
    )

    # Calculate shares profit
    shares_profit.calculate()

    # End calculation time measurement
    time_end = time.perf_counter()

    if term_short is True:
        print_str += term.get_stocks_investment_info(shares_profit, currency_symbol)
        print_str += term.get_total_investment_info(shares_profit, currency_symbol)
    if term_detailed is True:
        print_str += term.get_shares_investment_info(shares_profit, currency_symbol)

    if mail_short is True:
        mail_str += mail.get_stocks_investment_info(shares_profit, currency_symbol)
        mail_str += mail.get_total_investment_info(shares_profit, currency_symbol)
    if mail_detailed is True:
        mail_str += mail.get_shares_investment_info(shares_profit, currency_symbol)

    if print_str != "":
        print(print_str)
    if mail_str != "":
        shares_profit.mail.send(
            shares_profit.mail_config["send_to"], "Shares info", mail_str
        )

    # Print calculation time
    print(f"Calculation time: {time_end - time_start:0.2f} s")


if __name__ == "__main__":
    typer.run(main)
