# https://stackoverflow.com/a/46188210/14246508
# https://stackoverflow.com/a/59335943/14246508
# https://medium.com/stack-me-up/using-makefiles-the-right-way-a82091286950

COVERAGE ?= coverage
FLAKE8 ?= flake8
PY ?= python
BLACK ?= black
ISORT ?= isort
SCRIPT_NAME = packageLaunch.py
TESTS_FOLDER = tests
FOLDER_SRCS = src
SCRIPT_PARAM ?=
INSTALL_SCRIPT = freshInstall.py
SOURCE ?= .
REQUIREMENTS_PROD = requirements.prod.txt
REQUIREMENTS_DEV = requirements.dev.txt
VENV_DIR_PROD ?= .venv.prod
VENV_DIR_DEV ?= .venv.dev

DOCKER ?= docker
CONTAINER ?= shares-profit-calc
CONTAINER_BUILD ?= ./container_build.sh
CONTAINER_START ?= ./container_start.sh

.DEFAULT_GOAL := run


run: venv_prod
	@( \
		$(SOURCE) $(VENV_DIR_PROD)/bin/activate; \
		$(PY) $(SCRIPT_NAME) $(SCRIPT_PARAM); \
	)


code_formatting: venv_dev
	@( \
		$(SOURCE) $(VENV_DIR_DEV)/bin/activate; \
		$(ISORT) *.py $(FOLDER_SRCS)/*.py $(TESTS_FOLDER)/*.py; \
		$(BLACK) *.py $(FOLDER_SRCS)/*.py $(TESTS_FOLDER)/*.py; \
	)


# Check code for best standards
# flake8 --ignore=E501,F401 --max-complexity 10 --exclude .venv,.git,__pycache__ .
check: venv_dev
	@echo Checking code standards...
	@( \
		$(SOURCE) $(VENV_DIR_DEV)/bin/activate; \
		$(FLAKE8) .; \
	)



# @$(foreach file, \
# $(ALL_FILES_SH), \
# 	echo "#################################################"; \
# 	echo "Checking $(file)";  \
# 	echo "#################################################"; \
# 	shellcheck $(file) || exit 1; \
# )

.PHONY: test
test: venv_dev
	@( \
		$(SOURCE) $(VENV_DIR_DEV)/bin/activate; \
		$(COVERAGE) run -m unittest $(TESTS_FOLDER)/*.py; \
		$(COVERAGE) report -m; \
	)


venv_dev: $(VENV_DIR_DEV)/touchfile
venv_prod: $(VENV_DIR_PROD)/touchfile


# Create .venv if it doesn't exist - `test -d .venv.dev || python -m venv .venv.dev`
# Activate venv and install requirements inside - `source .venv/bin/activate && pip install -r requirements.txt
# Create `.venv/touchfile` so that this is ran only if requirements file changes
$(VENV_DIR_DEV)/touchfile: $(REQUIREMENTS_DEV)
	test -d $(VENV_DIR_DEV) || $(PY) -m venv $(VENV_DIR_DEV)
	$(SOURCE) $(VENV_DIR_DEV)/bin/activate && $(PY) -m pip install -r $(REQUIREMENTS_DEV)
	touch $(VENV_DIR_DEV)/touchfile


# Create .venv if it doesn't exist - `test -d .venv.dev || python -m venv .venv.dev`
# Activate venv and install requirements inside - `source .venv/bin/activate && pip install -r requirements.txt
# Create `.venv/touchfile` so that this is ran only if requirements file changes
$(VENV_DIR_PROD)/touchfile: $(REQUIREMENTS_PROD)
	test -d $(VENV_DIR_PROD) || $(PY) -m venv $(VENV_DIR_PROD)
	$(SOURCE) $(VENV_DIR_PROD)/bin/activate && $(PY) -m pip install -r $(REQUIREMENTS_PROD)
	touch $(VENV_DIR_PROD)/touchfile


.PHONY: commit
commit: check
	git commit


.PHONY: install
install: $(VENV_DIR_PROD)/touchfile
	git submodule update --init --recursive
	$(PY) $(INSTALL_SCRIPT)


container_stop:
	-$(DOCKER) stop $(CONTAINER)
.PHONY:container_stop

container_rm:
	-$(DOCKER) rm $(CONTAINER)
.PHONY:container_rm

container_build: 
	$(CONTAINER_BUILD)
.PHONY:container_build

container_start: container_stop container_rm
	$(CONTAINER_START)
.PHONY:container_start

container_logs:
	$(DOCKER) logs -f $(CONTAINER)
.PHONY:container_logs

container_enter:
	$(shell $(DOCKER) container ls | grep $(CONTAINER) | awk '{ printf("$(DOCKER) exec -it %s bash", $$1)}')
.PHONY:container_enter
