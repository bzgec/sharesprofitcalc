#!/usr/bin/env bash

set -euo pipefail  # https://gist.github.com/maxisam/e39efe89455d26b75999418bf60cf56c

docker build --tag shares-profit-calc -f Dockerfile .
