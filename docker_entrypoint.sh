#!/usr/bin/env bash

echo ""
echo "##########################################################"
date '+Container started: %Y/%m/%d %H:%M:%S'
echo "##########################################################"

# Turn on bash's job control
set -m

# Start crontab
cron -f -l 2 &

# Setup crontab file - cron jobs
/bin/sh docker_checkCrontabFile.sh

# Now we bring the primary process back into the foreground
# and leave it there
fg %1
