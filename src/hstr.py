#!/usr/bin/env python3

import pandas as pd
import matplotlib.pyplot as plt
import yahooquery as yfq
from rich import print


def plot_history_data(
    pandas_df,
    title: str,
    show: bool = False,
    save_to_file: bool = False,
    file_name: str = "history_data",
) -> None:
    '''Plot history data from pandas DataFrame'''
    plt.figure()

    pandas_df.plot()
    plt.legend()
    plt.title(title)

    if save_to_file is True:
        plt.savefig(file_name + ".png")

    if show is True:
        plt.show()


ticker_1 = yfq.Ticker("AAPL GOOG", asynchronous=True)
ticker_2 = yfq.Ticker("IWDA.AS", asynchronous=True)

# https://yahooquery.dpguthrie.com/guide/ticker/historical/
# period: ['1d', '5d', '7d', '60d', '1mo', '3mo', '6mo', '1y', '2y', '5y', '10y', 'ytd', 'max']
# interval: ['1m', '2m', '5m', '15m', '30m', '60m', '90m', '1h', '1d', '5d', '1wk', '1mo', '3mo']
pandas_df_1 = ticker_1.history(period="1mo", interval="1d")
pandas_df_2 = ticker_2.history(period="1mo", interval="1d")
#print(pandas_df_1)

new_df_1 = pandas_df_1.pivot_table(index="date", columns="symbol", values="close")
new_df_2 = pandas_df_2.pivot_table(index="date", columns="symbol", values="close")

#pandas_df = pd.concat([pandas_df_1.close, pandas_df_2.close])
#pandas_df = new_df_1.merge(new_df_2, left_on='date', right_on='date')
pandas_df = new_df_1.join(new_df_2, on='date')
print(pandas_df)

# https://pandas.pydata.org/docs/user_guide/reshaping.html
#new_df = pandas_df.pivot_table(index="date", columns="symbol", values="close")
#new_df = pandas_df.pivot_table(index="date", columns="symbol")
#print(new_df)

##new_df["proceeds"] = new_df["AAPL"] + new_df["IWDA.AS"]
##print(new_df)
pandas_df["proceeds"] = pandas_df["AAPL"] + pandas_df["IWDA.AS"]
print(pandas_df)
#
##plot_history_data(new_df.proceeds, "Portfolio value", show=True)
plot_history_data(pandas_df.proceeds, "Portfolio value", show=True)
## plot_history_data(, "Proceeds", "proceeds")
## new_df.plot()
## plt.show()
