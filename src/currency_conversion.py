#!/usr/bin/env python

# Standard libraries
import csv
import os
import urllib.request
from datetime import date

# 3rd party packages

# Local source

DB_FOLDER = (
    "/home/bzgec/projects/homeAutomation/myServer/sharesProfitCalc/currency-conv-db"
)


class UnsupportedCurrencyError(Exception):
    """Currency not supported error"""

    pass


class UnsupportedDatabaseNameError(Exception):
    """Database name must end with '.csv'"""

    pass


class DateOutOfRangeError(Exception):
    """Date is out of range"""

    pass


# https://data.ecb.europa.eu/help/api/data
# D - the frequency at which they are measured - daily
# USD - the currency being measured
# EUR - the currency against which the above currency is being measured
# SP00 - the type of exchange rates - foreign exchange reference rates
# A - the Time series variation - average or standardised measure for a given frequency
# LINK_CURRENCY_CONVERSIONS_USD = "https://data-api.ecb.europa.eu/service/data/EXR/D.USD.EUR.SP00.A?startPeriod=2023-10-01&endPeriod=2023-11-23&format=csvdata"
LINK_STD_FMT = "https://data-api.ecb.europa.eu/service/data/EXR/D.{currency:s}.EUR.SP00.A?format=csvdata"

CURRENCIES = [
    "USD",
    "JPY",
    "GBP",
    "CNY",
    "TWD",
    "CAD",
]


def setup_link_currency(currency: str) -> str:
    """Create database download link for specified currency"""
    return LINK_STD_FMT.format(currency=currency)


def setup_link_dates(link: str, date_start: date, date_end: date) -> str:
    """Add date parameters to download db link"""
    link += f"&startPeriod={date_start.isoformat()}&endPeriod={date_end.isoformat()}"
    return link


def download_csv(link: str, file_name: str) -> None:
    """Download database"""
    urllib.request.urlretrieve(link, file_name)


def remove_unused_data(db_orig: str, db_modified: str, currency: str) -> None:
    """Convert downloaded database to database used by this program"""
    fieldnames_orig = {
        "currency-to": "CURRENCY",
        "currency-base": "CURRENCY_DENOM",
        "date": "TIME_PERIOD",
        "conversion-rate": "OBS_VALUE",
    }

    with open(db_modified, "w") as csvfile_mod:
        csvfile_mod.write(f"Date;EUR-to-{currency}\n")
        with open(db_orig, newline="") as csvfile_orig:
            reader = csv.DictReader(csvfile_orig, delimiter=",", quotechar='"')
            for row in reader:
                # print(
                #     f'Date: {row[fieldnames_orig["date"]]}, '
                #     f'Currency from: {row[fieldnames_orig["currency-base"]]}, '
                #     f'Currency to: {row[fieldnames_orig["currency-to"]]}, '
                #     f'Rate: {float(row[fieldnames_orig["conversion-rate"]]):01.4f}'
                # )
                csvfile_mod.write(
                    f'{row[fieldnames_orig["date"]]}'
                    ";"
                    f'{row[fieldnames_orig["conversion-rate"]]}'
                    "\n"
                )


def update_db(db_mod: str, currency: str, date_start: date) -> None:
    """Update database from start date to current date"""
    if currency not in CURRENCIES:
        raise UnsupportedCurrencyError(f'Currency "{currency}" is not supported')

    if ".csv" != db_mod[::-1][0:4][::-1]:
        raise UnsupportedDatabaseNameError('Database name must end with ".csv"')

    download_link = setup_link_currency(currency)
    download_link = setup_link_dates(download_link, date_start, date.today())

    db_orig = db_mod[:-4] + "-orig.csv"
    download_csv(download_link, db_orig)
    remove_unused_data(db_orig, db_mod, currency)


def get_conversion_rate_db(db_mod: str, currency: str) -> [(str, str)]:
    """
    Get conversion rate database from file.
    `currency` parameter is used because the way database data is stored and also
    to be sure that specified database is really for desired currency.
    """
    if currency not in CURRENCIES:
        raise UnsupportedCurrencyError(f'Currency "{currency}" is not supported')

    db_data = []

    # Read the database into a variable
    conversion_col = f"EUR-to-{currency}"
    with open(db_mod, newline="") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=";", quotechar='"')
        for row in reader:
            db_data.append((row["Date"], row[conversion_col]))
            # print(row)
            # print(f'Date: {row["Date"]}, ' f"Conversion rate: {row[conversion_col]}")

    return db_data


def get_conversion_rate(db_data: [(str, str)], conv_date: date) -> float:
    """
    Get conversion rate from database for specific date.
    If specified date is not in database former available date is selected.
    If date is more than 14 days bigger than last day stored in db error is raised.
    """

    db_len = len(db_data) - 1

    if conv_date < date.fromisoformat(db_data[0][0]):
        raise DateOutOfRangeError(
            f"Date {conv_date} is lower than the oldest date stored in file ({db_data[0][0]})"
        )
    elif conv_date > date.fromisoformat(db_data[db_len][0]):
        if (conv_date - date.fromisoformat(db_data[db_len][0])).days > 14:
            raise DateOutOfRangeError(
                f"Date {conv_date} is 14+ days bigger than the newest date stored in file ({db_data[db_len][0]})"
            )
        else:
            # Day is bigger than stored in file, but not too big
            # (there is no currency conversion data on weekends and holidays...)
            return float(db_data[db_len][1])

    # Binary search the desired date
    R = db_len
    L = 0
    m = 0

    while L <= R:
        m = (R + L) // 2
        if date.fromisoformat(db_data[m][0]) < conv_date:
            L = m + 1
        elif date.fromisoformat(db_data[m][0]) > conv_date:
            R = m - 1
        else:
            # Match found
            break

    if date.fromisoformat(db_data[m][0]) == conv_date:
        return float(db_data[m][1])
    else:
        # Date not found, select previous date
        return float(db_data[m - 1][1])


def main():
    os.makedirs(DB_FOLDER, exist_ok=True)
    date_start = date(2021, 5, 28)

    for currency in CURRENCIES:
        db_mod = f"{DB_FOLDER}/db-{currency}.csv"
        update_db(db_mod, currency, date_start)


if __name__ == "__main__":
    main()
