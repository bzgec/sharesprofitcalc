"""
Prepare strings for printing to terminal or sending via email
"""

# Standard libraries
from collections.abc import Callable

# Local source
from src.shares_profit_calc import CurrencySymbols, SharesToUse

# 3rd party packages


NEW_LINE_TERM = "\n"
NEW_LINE_MAIL = "<br>"
SEPARATOR_TERM = "################################################################\n"
SEPARATOR_MAIL = "<hr>"


def sort_by_total_real_proceeds(shares_profit) -> dict:
    """Return sorted dictionary by total real proceeds per stock - sort from highest to lowest"""
    return dict(
        sorted(
            shares_profit.total_real_proceeds_per_stock.items(),
            key=lambda x: x[1],
            reverse=True,
        )
    )


def get_total_investment_info(
    shares_profit,
    script_currency_symbol: str,
    new_line_symbol: str,
    separator: str,
    color_text_red_green_fn: Callable,
    bold_text_fn: Callable,
) -> str:
    """Get total investment information string (can be used for terminal of mail)"""
    return_str = ""
    return_str += (
        f"{bold_text_fn('Total proceeds')}: "
        + f"{shares_profit.total_proceeds:0.0f} {script_currency_symbol}"
        + f"{new_line_symbol}"
    )
    return_str += (
        f"{bold_text_fn('Total real proceeds')}: "
        + f"{shares_profit.total_real_proceeds:0.0f} {script_currency_symbol}"
        + f"{new_line_symbol}"
    )
    return_str += (
        f"{bold_text_fn('Total investment')}: "
        + f"{shares_profit.total_investment:0.0f} {script_currency_symbol}"
        + f"{new_line_symbol}"
    )
    return_str += (
        f"{bold_text_fn('Total net profit')}: "
        + f"{color_text_red_green_fn(shares_profit.total_net_profit)} {script_currency_symbol}"
        + f"{new_line_symbol}"
    )
    return_str += (
        f"{bold_text_fn('Total return on investment')}: "
        + f"{color_text_red_green_fn(shares_profit.total_return_on_investment)} %"
        + f"{new_line_symbol}"
    )
    return_str += separator

    return return_str


def prep_share_str(
    share: dict,
    script_currency_symbol: str,
    new_line_symbol: str,
    separator: str,
    color_text_red_green_fn: Callable,
    bold_text_fn: Callable,
) -> str:
    """Prepare detailed share information string (can be used for terminal or mail)"""
    share_currency_symbol = CurrencySymbols[share["currency"]]
    return_str = ""
    return_str += (
        f'{bold_text_fn(share["symbol"])}: '
        + f'{color_text_red_green_fn(share["net_profit"])} '
        + f'({color_text_red_green_fn(share["return_on_investment"])}) '
        + f"{new_line_symbol}"
    )
    return_str += f'Company: {share["company"]} {new_line_symbol}'
    return_str += f'Buy date: {share["date_buy"]} {new_line_symbol}'
    return_str += f'Buy price: {share["price_buy"]:0.1f}'
    return_str += f" {share_currency_symbol} {new_line_symbol}"

    if share["price_sell"] != 0:
        return_str += f'Sell date: {share["date_sell"]} {new_line_symbol}'
        return_str += f'Sell price: {share["price_sell"]:0.1f}'
        return_str += f" {share_currency_symbol} {new_line_symbol}"
    else:
        return_str += f'Current price: {share["price_end"]:0.1f}'
        return_str += f" {share_currency_symbol} {new_line_symbol}"

    return_str += f'Initial investment: {share["investment_initial"]:0.1f}'
    return_str += f" {script_currency_symbol} {new_line_symbol}"
    return_str += f'Total investment: {share["investment_total"]:0.1f}'
    return_str += f" {script_currency_symbol} {new_line_symbol}"
    return_str += f'Net profit: {color_text_red_green_fn(share["net_profit"])}'
    return_str += f" {script_currency_symbol} {new_line_symbol}"
    return_str += f'Return on investment: {color_text_red_green_fn(share["return_on_investment"])}'
    return_str += f" % {new_line_symbol}"
    return_str += f'Tax: {share["tax"]:0.1f}'
    return_str += f" {script_currency_symbol} {new_line_symbol}"
    return_str += (
        f'To break even share price should be: {share["break_even_price"]:0.1f}'
    )
    return_str += f" {share_currency_symbol} {new_line_symbol}"
    return_str += f'Share proceeds (real): {share["proceeds"]:0.1f} ({share["proceeds_real"]:0.1f})'
    return_str += f" {script_currency_symbol} {new_line_symbol}"
    return_str += (
        "Share proceeds / total proceeds (real): "
        + f'{share["share_perc_per_total_proceeds"]:0.1f} '
        + f'({share["share_perc_per_total_real_proceeds"]:0.1f})'
    )
    return_str += f" % {new_line_symbol}"
    return_str += (
        "Stock proceeds (real): "
        + f'{share["stock_proceeds"]:0.1f} ({share["stock_proceeds_real"]:0.1f})'
    )
    return_str += f" {script_currency_symbol} {new_line_symbol}"
    return_str += (
        "Stock proceeds / total proceeds (real): "
        + f'{share["stock_perc_per_total_proceeds"]:0.1f} '
        + f'({share["stock_perc_per_total_real_proceeds"]:0.1f})'
    )
    return_str += f" % {new_line_symbol}"
    return_str += f"{separator}"

    return return_str


def get_detailed_shares_info(
    shares_profit,
    script_currency_symbol: str,
    new_line_symbol: str,
    separator: str,
    color_text_red_green_fn: Callable,
    bold_text_fn: Callable,
) -> str:
    """Get detailed shares information string (can be used for terminal or mail)"""
    return_str = ""
    for share in shares_profit.shares_list:
        if shares_profit.shares_to_use == SharesToUse.ALL:
            return_str += prep_share_str(
                share,
                script_currency_symbol,
                new_line_symbol,
                separator,
                color_text_red_green_fn,
                bold_text_fn,
            )
        elif (
            shares_profit.shares_to_use == SharesToUse.ONLY_HOLDING
            and share["price_sell"] == 0
        ):
            return_str += prep_share_str(
                share,
                script_currency_symbol,
                new_line_symbol,
                separator,
                color_text_red_green_fn,
                bold_text_fn,
            )
        elif (
            shares_profit.shares_to_use == SharesToUse.ONLY_SOLD
            and share["price_sell"] != 0
        ):
            return_str += prep_share_str(
                share,
                script_currency_symbol,
                new_line_symbol,
                separator,
                color_text_red_green_fn,
                bold_text_fn,
            )

    return return_str


class FormatTextTerm:
    """Format text which is printed to terminal"""

    def __init__(self):
        self.new_line_symbol = NEW_LINE_TERM
        self.separator_line = SEPARATOR_TERM

    def bold_text(self, text: str) -> str:
        """Format text in bold"""
        return f"[bold]{text}[/bold]"

    def red_text(self, text: str) -> str:
        """Format text in red color"""
        return f"[red]{text}[/red]"

    def green_text(self, text: str) -> str:
        """Format text in green color"""
        return f"[green]{text}[/green]"

    def red_or_green(self, numb: str | float | int) -> str:
        """Format text in red or green color depending if number is positive or negative"""
        return_str = ""

        if isinstance(numb, str):
            if float(numb) >= 0:
                return_str = self.green_text(numb)
            else:
                return_str = self.red_text(numb)
        else:
            if numb >= 0:
                return_str = self.green_text(f"{numb:+0.1f}")
            else:
                return_str = self.red_text(f"{numb:+0.1f}")

        return return_str


class FormatTextMail:
    """Format text which is send via mail"""

    def __init__(self):
        self.new_line_symbol = NEW_LINE_MAIL
        self.separator_line = SEPARATOR_MAIL

    def bold_text(self, text: str) -> str:
        """Format text in bold"""
        return f"<b>{text}</b>"

    def red_text(self, text: str) -> str:
        """Format text in red color"""
        return f'<span style="color:red">{text}</span>'

    def green_text(self, text: str) -> str:
        """Format text in green color"""
        return f'<span style="color:green">{text}</span>'

    def red_or_green(self, numb: float | int) -> str:
        """Format text in red or green color depending if number is positive or negative"""
        if numb >= 0:
            return self.green_text(f"{numb:+0.1f}")

        return self.red_text(f"{numb:+0.1f}")


class Term(FormatTextTerm):
    def __init__(self):
        FormatTextTerm.__init__(self)

    def get_total_investment_info(
        self, shares_profit, script_currency_symbol: str
    ) -> str:
        """Get total investment information string"""
        return get_total_investment_info(
            shares_profit,
            script_currency_symbol,
            self.new_line_symbol,
            self.separator_line,
            self.red_or_green,
            self.bold_text,
        )

    def get_stocks_investment_info(self, shares_profit, currency_symbol: str) -> str:
        """
        Get stock investment information in a table, sorted by highest real stock proceeds string
        """
        sorted_stocks_dict = sort_by_total_real_proceeds(shares_profit)
        return_str = ""
        return_str += "+----------+--------------+----------------------+----------------+---------+\n"
        return_str += "| Name     "
        return_str += "| Price        "
        return_str += f"| Real proceeds [{currency_symbol}(%)] "
        return_str += f"| Net profit [{currency_symbol}] "
        return_str += "| ROI [%] |\n"
        return_str += "+==========+==============+======================+================+=========+\n"
        for stock in sorted_stocks_dict:
            return_str += f"| {stock:8s} "
            return_str += f"| {shares_profit.price_end[stock]:8.2f} {shares_profit.stock_currency[stock]:3s} "
            return_str += (
                f"| {shares_profit.total_real_proceeds_per_stock[stock]:12.1f} "
                + f"({shares_profit.stock_perc_per_total_real_proceeds[stock]:5.1f}) "
            )
            tmp_str = f"{shares_profit.stock_net_profit[stock]:+14.1f}"
            return_str += f"| {self.red_or_green(tmp_str)} "
            tmp_str = f"{shares_profit.stock_return_on_investment[stock]:+7.1f}"
            return_str += f"| {self.red_or_green(tmp_str)} |\n"
        return_str += "+----------+--------------+----------------------+----------------+---------+\n"

        return return_str

    def get_shares_investment_info(
        self, shares_profit, script_currency_symbol: str
    ) -> str:
        """Get detailed shares information string"""
        return get_detailed_shares_info(
            shares_profit,
            script_currency_symbol,
            self.new_line_symbol,
            self.separator_line,
            self.red_or_green,
            self.bold_text,
        )


class Mail(FormatTextMail):
    def __init__(self):
        FormatTextMail.__init__(self)

    def get_total_investment_info(
        self, shares_profit, script_currency_symbol: str
    ) -> str:
        """Get total investment information string"""
        return get_total_investment_info(
            shares_profit,
            script_currency_symbol,
            self.new_line_symbol,
            self.separator_line,
            self.red_or_green,
            self.bold_text,
        )

    def get_stocks_investment_info(self, shares_profit, currency_symbol: str) -> str:
        """
        Get stock investment information in a table, sorted by highest real stock proceeds string
        """

        sorted_stocks_dict = sort_by_total_real_proceeds(shares_profit)

        style_th = (
            'style="border:1px solid #b3adad; padding:5px; background: #f0f0f0; '
            + 'color: #313030; text-align: left;"'
        )
        style_td_text = (
            'style="border:1px solid #b3adad; padding:3px; background: #ffffff; '
            + 'color: #313030; text-align: left;"'
        )
        style_td_numb = (
            'style="border:1px solid #b3adad; padding:3px; background: #ffffff; '
            + 'color: #313030; text-align: right;"'
        )

        def th(th_data: str) -> str:
            return f"<th {style_th}>{th_data}</th>"

        def td_text(td_data: str) -> str:
            return f"<td {style_td_text}>{td_data}</td>"

        def td_numb(td_data: str) -> str:
            return f"<td {style_td_numb}>{td_data}</td>"

        return_str = ""
        return_str += (
            '<table style="border:1px solid #b3adad; border-collapse:collapse; padding:5px;">'
            "<thead>"
            "<tr>"
            f"<th {style_th}>Name</th>"
            f"<th {style_th}>Price</th>"
            f"<th {style_th}>Real proceeds [{currency_symbol}(%)]</th>"
            f"<th {style_th}>Net profit [{currency_symbol}]</th>"
            f"<th {style_th}>ROI [%]</th>"
            "</tr>"
            "</thead>"
            "<tbody>"
        )

        for stock in sorted_stocks_dict:
            return_str += "<tr>"
            return_str += td_text(stock)
            return_str += td_numb(
                    f"{shares_profit.price_end[stock]:0.2f} {shares_profit.stock_currency[stock]}"
            )
            return_str += td_numb(
                f"{shares_profit.total_real_proceeds_per_stock[stock]:0.1f} "
                + f"({shares_profit.stock_perc_per_total_real_proceeds[stock]:0.1f})"
            )
            return_str += td_numb(
                f"{self.red_or_green(shares_profit.stock_net_profit[stock])}"
            )
            return_str += td_numb(
                f"{self.red_or_green(shares_profit.stock_return_on_investment[stock])}"
            )
            return_str += "</tr>"

        return_str += "</tbody>" "</table>"

        return return_str

    def get_shares_investment_info(
        self, shares_profit, script_currency_symbol: str
    ) -> str:
        """Get detailed shares information string"""
        return get_detailed_shares_info(
            shares_profit,
            script_currency_symbol,
            self.new_line_symbol,
            self.separator_line,
            self.red_or_green,
            self.bold_text,
        )
