#!/usr/bin/env python

"""
handle["shares_list"]:

Specified by the user

+-------------------+---------------+-------------------------------------------+
| Key               | Typical value | Details                                   |
+===================+===============+===========================================+
| "symbol"          | "AAPL"        | Stock symbol                              |
+-------------------+---------------+-------------------------------------------+
| "company"         | "Apple Inc."  | Full company name                         |
+-------------------+---------------+-------------------------------------------+
| "numb_of_shares"  | 2             | Number of shares bought                   |
+-------------------+---------------+-------------------------------------------+
| "currency"        | "USD"         | Currency with which the share is bought   |
|                   |               | and sold                                  |
|                   |               | (commissions are also in this currency)   |
+-------------------+---------------+-------------------------------------------+
| "price_buy"       | 100           | Price at which the share was bought       |
+-------------------+---------------+-------------------------------------------+
| "price_sell"      | 150           | Price at which the share was sold         |
|                   |               | (MUST BE 0 if share is not yet sold)      |
+-------------------+---------------+-------------------------------------------+
| "date_buy"        | "2021/05/28"  | Date the share was bought                 |
+-------------------+---------------+-------------------------------------------+
| "date_sell"       | "2021/07/09"  | Date the share was sold                   |
|                   |               | (MUST BE empty string if                  |
|                   |               | share is not yet sold -> "")              |
+-------------------+---------------+-------------------------------------------+
| "commission_buy"  | 0.35          | Commission for buying the share           |
+-------------------+---------------+-------------------------------------------+
| "commission_sell" | 0.37          | Commission for selling the share          |
+-------------------+---------------+-------------------------------------------+
| "cap_gain_tax"    | 27.5          | Capital gain tax                          |
+-------------------+---------------+-------------------------------------------+

Calculated later
+--------------------------------------+-------------------------------------------+
| Key                                  | Details                                   |
+======================================+===========================================+
| "price_end"                          | Current stock price or price at which     |
|                                      | stock was sold      [share["currency"]]   |
+--------------------------------------+-------------------------------------------+
| "break_even_price"                   | Price of stock at which this share is     |
|                                      | profitable [share["currency"]]            |
+--------------------------------------+-------------------------------------------+
| "investment_initial"                 | Initial investment                        |
|                                      | (paid for share plus buy commission)      |
|                                      | [script_currency]                         |
+--------------------------------------+-------------------------------------------+
| "investment_total"                   | Total investment                          |
|                                      | (paid for share plus including all        |
|                                      | commissions) [script_currency]            |
+--------------------------------------+-------------------------------------------+
| "tax"                                | Tax paid when sold [script_currency]      |
+--------------------------------------+-------------------------------------------+
| "net_profit"                         | Net profit (minus tax) [script_currency]  |
+--------------------------------------+-------------------------------------------+
| "return_on_investment"               | Return on investment (minus tax) [%]      |
+--------------------------------------+-------------------------------------------+
| "proceeds"                           | Current shares value [script_currency]    |
+--------------------------------------+-------------------------------------------+
| "proceeds_real"                      | Proceeds minus commission_sell minus tax  |
|                                      | [script_currency]                         |
+--------------------------------------+-------------------------------------------+
| "share_perc_per_total_proceeds"      | Share percentages against total proceeds  |
|                                      | [%]                                       |
|                                      | - dependent on shares_to_use              |
+--------------------------------------+-------------------------------------------+
| "share_perc_per_total_real_proceeds" | Share percentages against total real      |
|                                      | proceeds [%]                              |
|                                      | - dependent on shares_to_use              |
+--------------------------------------+-------------------------------------------+
| "stock_proceeds"                     | All shares of same stock value            |
|                                      | [script currency]                         |
|                                      | - dependent on shares_to_use              |
+--------------------------------------+-------------------------------------------+
| "stock_proceeds_real"                | All shares of same stock value            |
|                                      | minus commission_sell minus tax           |
|                                      | [script currency]                         |
|                                      | - dependent on shares_to_use              |
+--------------------------------------+-------------------------------------------+
| "stock_perc_per_total_proceeds"      | Stock percentages against total proceeds  |
|                                      | [%]                                       |
|                                      | - dependent on shares_to_use              |
+--------------------------------------+-------------------------------------------+
| "stock_perc_per_total_real_proceeds" | Stock percentages against total real      |
|                                      | proceeds [%]                              |
|                                      | - dependent on shares_to_use              |
+--------------------------------------+-------------------------------------------+
"""

import json
# Standard libraries
import sys
import time
import traceback
from collections.abc import Callable
from datetime import date, datetime
from enum import Enum
from random import random
from threading import Thread
from typing import Any
from urllib.request import urlopen

# 3rd party packages
import yahooquery as yfq
from rich import print

from . import currency_conversion as cc
# Local source
from .helperLib import mail_sender


class SharesToUse(Enum):
    """Which shares to use"""

    ALL = 0
    ONLY_HOLDING = 1
    ONLY_SOLD = 2


class Currency(Enum):
    """Currency symbols enumerator"""

    EUR = "EUR"
    USD = "USD"
    GBP = "GBP"


# Currency symbols enumerator
CurrencySymbols = {"EUR": "€", "USD": "$"}


class SharesProfit:
    def __init__(
        self,
        shares_list: list[dict],
        mail_config: dict = {},
        currency: Currency = Currency.EUR,
        shares_to_use: SharesToUse = SharesToUse.ALL,
    ):
        # Check that shares_list is not empty
        if not shares_list:
            raise AttributeError('"shares_list" argument is empty')

        # Check that mail_config is ok
        if mail_config:
            if "sender_mail" not in mail_config:
                raise AttributeError('No "sender_mail" in mail configuration')
            if "sender_pass" not in mail_config:
                raise AttributeError('No "sender_pass" in mail configuration')
            if "send_to" not in mail_config:
                raise AttributeError('No "send_to" in mail configuration')

        self.shares_list = shares_list
        self.mail_config = mail_config
        self.shares_to_use = shares_to_use
        self.currency = currency
        self.currency_conversion = {
            "base": "EUR",
            "db": {},
        }
        self.total_proceeds = 0  # Dependent on: shares_to_use, currency
        self.total_real_proceeds = 0  # Dependent on: shares_to_use, currency
        self.total_proceeds_per_stock = {}  # Dependent on: shares_to_use, currency
        self.total_real_proceeds_per_stock = {}  # Dependent on: shares_to_use, currency
        self.stock_perc_per_total_proceeds = {}  # Dependent on: shares_to_use, currency
        self.stock_perc_per_total_real_proceeds = (
            {}
        )  # Dependent on: shares_to_use, currency
        self.stock_net_profit = {}  # Dependent on: shares_to_use, currency
        self.total_net_profit = 0  # Dependent on: shares_to_use, currency
        self.stock_investment = {}  # Dependent on: shares_to_use, currency
        self.total_investment = 0  # Dependent on: shares_to_use, currency
        self.stock_return_on_investment = {}  # Dependent on: shares_to_use, currency
        self.total_return_on_investment = 0  # Dependent on: shares_to_use, currency

        if self.mail_config:
            self.mail = mail_sender.Mail(
                self.mail_config["sender_mail"], self.mail_config["sender_pass"]
            )

        # Get currency conversion rates
        self.get_currency_conversion_rates()

        # Get latest stock prices
        self.price_end = self.get_end_stock_price()

        # Set stock currencies
        self.stock_currency = self.set_stock_currencies(self.price_end)

    def calculate(self):
        # Calculate shares profit
        self.calc_shares_data()

        # Get total (real) proceeds in script currency
        self.total_proceeds = self.get_total_proceeds()
        self.total_real_proceeds = self.get_total_real_proceeds()

        # Get share percentages per total (real) proceeds
        for share in self.shares_list:
            self.set_share_perc_per_total_proceeds(share)
            self.set_share_perc_per_total_real_proceeds(share)

        # Get total (real) proceeds per stock
        self.total_proceeds_per_stock = self.get_total_proceeds_per_stock()
        self.total_real_proceeds_per_stock = self.get_total_real_proceeds_per_stock()

        # Get stock and total net profit
        self.stock_net_profit = self.get_stock_net_profit()
        self.total_net_profit = self.get_total_net_profit()

        # Stock and total investment
        self.stock_investment = self.get_stock_investment()
        self.total_investment = self.get_total_investment()

        # Stock and total return on investment
        self.stock_return_on_investment = self.get_stock_return_on_investment()
        self.total_return_on_investment = self.get_total_return_on_investment()

        # Get percentages per stock against total (real) proceeds
        self.stock_perc_per_total_proceeds = self.get_stock_perc_per_total_proceeds()
        self.stock_perc_per_total_real_proceeds = (
            self.get_stock_perc_per_total_real_proceeds()
        )

        # Add total (real) proceeds per stock to shares_list
        self.add_total_stock_proceeds_to_shares_list()
        self.add_total_real_stock_proceeds_to_shares_list()
        self.add_stock_perc_per_total_proceeds_to_shares_list()
        self.add_stock_perc_per_total_real_proceeds_to_shares_list()

    def convert_date(self, date_str: str) -> date:
        return datetime.strptime(date_str, "%Y/%m/%d").date()

    def get_currency_conversion_rates(self) -> None:
        oldest_date = {}
        currencies = []

        for share in self.shares_list:
            # Find currencies
            share_currency = share["currency"]
            if share_currency not in currencies:
                currencies.append(share["currency"])
                oldest_date[share_currency] = date.today()

            # Find oldest date in shares_list
            share_date = self.convert_date(share["date_buy"])
            if oldest_date[share_currency] > share_date:
                oldest_date[share_currency] = share_date

        for currency in currencies:
            if currency == "EUR":
                # "EUR" is base currency for calculations
                continue

            cc.update_db(f"db-{currency}.csv", currency, oldest_date[currency])
            self.currency_conversion["db"][currency] = cc.get_conversion_rate_db(
                f"db-{currency}.csv", currency
            )

    def get_end_stock_price(self) -> dict:
        """
        Get and set:
        - current share price for each holding share
        - and sell price for each sold share
        """
        # Get list of holding stocks
        holding_stocks = self.get_holding_stocks()

        if self.shares_to_use == SharesToUse.ONLY_SOLD:
            stock_price = {}
        else:
            # Scrape stock prices
            stock_price = self.scrape_stocks_price(holding_stocks)

        # Set end stock price to shares
        self.set_end_stock_price_to_shares(stock_price)

        return stock_price

    def set_stock_currencies(self, stock_price) -> dict:
        """
        Set stock currencies for each stock
        """
        stock_currency = {}
        for stock in stock_price:
            for share in self.shares_list:
                if stock == share["symbol"]:
                    stock_currency[stock] = share["currency"]


        return stock_currency

    def get_holding_stocks(self) -> [str]:
        """Get holding stocks list"""
        stocks_list = []

        # Create list of holding stocks
        for share in self.shares_list:
            if share["date_sell"] != "":
                # Don't scrape price for sold shares
                pass
            elif share["symbol"] in stocks_list:
                pass
            else:
                stocks_list.append(share["symbol"])

        return stocks_list

    def scrape_stocks_price(self, stocks: [str]) -> dict:
        """Scrape all stock prices"""
        stock_price = {}
        error_stocks = []
        thread_handle = [None] * len(stocks)

        def scrape_stock_price(stock_name):
            try:
                stock_price[stock_name] = self.get_latest_stock_price(stock_name)
            except Exception:
                error_stocks.append(stock_name)
                print(traceback.format_exc())

        # Start scraping of each individual stock price
        for i in range(len(stocks)):
            thread_handle[i] = Thread(target=scrape_stock_price, args=(stocks[i],))
            thread_handle[i].start()

        # Wait until all stock prices are scraped
        for i in range(len(stocks)):
            thread_handle[i].join()

        # Check that there was no error when scraping for prices
        if len(error_stocks) != 0:
            print(
                f"Current price scraping not working: {error_stocks}", file=sys.stderr
            )
            sys.exit(1)

        # Check that we received price for all stocks
        for stock in stock_price:
            if stock_price[stock] == 0:
                raise ValueError(f'Stock price is 0 for stock: "{stock}"')

        return stock_price

    def get_latest_stock_price(self, stock_symbol: str) -> float:
        """Get current stock price"""
        ticker = yfq.Ticker(stock_symbol)
        return ticker.price[stock_symbol]["regularMarketPrice"]

    def set_end_stock_price_to_shares(self, stock_prices: dict) -> None:
        """
        'price_end' for each share:
          - holding shares: current stock price
          - sold shares: price at which the share was sold
        """
        if self.shares_to_use != SharesToUse.ONLY_SOLD:
            # Set latest stock price to holding shares
            for share in self.shares_list:
                for stock_name in stock_prices:
                    if stock_name == share["symbol"]:
                        share["price_end"] = stock_prices[stock_name]
                        break
        else:
            # Set None to holding shares - holding shares are not used when ONLY_SOLD is used
            for share in self.shares_list:
                # Just set none for all shares (the one which are sold will be set later)
                share["price_end"] = 0

        # Set "price_sell" to "price_end" for sold shares
        for share in self.shares_list:
            if share["date_sell"] != "":
                share["price_end"] = share["price_sell"]
                if share["symbol"] not in stock_prices:
                    stock_prices[share["symbol"]] = share["price_end"]


    def calc_shares_data(self) -> list:
        def calc_share_data(self, share: dict) -> list:
            """
            Calculate share profit:
            - investment_initial
            - investment_total
            - break_even_price
            - tax
            - net_profit
            - return_on_investment
            - proceeds
            - proceeds_real
            """
            if share["date_sell"] == "":
                date_end = date.today()
            else:
                date_end = self.convert_date(share["date_sell"])

            # In share currency
            share["break_even_price"] = (
                share["price_buy"]
                + (share["commission_buy"] + share["commission_sell"])
                / share["numb_of_shares"]
            )

            # In script currency
            price_end = self.conv_currency(
                self.currency.value, share["currency"], share["price_end"], date_end
            )
            price_buy = self.conv_currency(
                self.currency.value,
                share["currency"],
                share["price_buy"],
                self.convert_date(share["date_buy"]),
            )
            commission_buy = self.conv_currency(
                self.currency.value,
                share["currency"],
                share["commission_buy"],
                self.convert_date(share["date_buy"]),
            )
            commission_sell = self.conv_currency(
                self.currency.value,
                share["currency"],
                share["commission_sell"],
                date_end,
            )

            share["investment_initial"] = (
                share["numb_of_shares"] * price_buy + commission_buy
            )
            share["investment_total"] = share["investment_initial"] + commission_sell

            profit = (price_end - price_buy) * share["numb_of_shares"]

            # TODO: should we include commissions in tax calculation
            share["tax"] = profit * share["cap_gain_tax"] / 100
            # Fix negative tax
            share["tax"] = max(share["tax"], 0)
            share["net_profit"] = (
                profit - commission_buy - commission_sell - share["tax"]
            )
            share["return_on_investment"] = (
                share["net_profit"]
                / (
                    (price_buy * share["numb_of_shares"])
                    + commission_buy
                    + commission_sell
                )
                * 100
            )

            share["proceeds"] = share["numb_of_shares"] * price_end
            share["proceeds_real"] = share["proceeds"] - commission_sell - share["tax"]

            return share

        for share in self.shares_list:
            calc_share_data(self, share)

    def conv_currency(
        self, currency_to: str, currency_from: str, value: float, conv_date: date
    ) -> float:
        """Convert currency"""
        converted_value = 0

        if currency_to == currency_from:
            # EUR -> EUR
            converted_value = value
        else:
            if currency_to == "EUR":
                # Convert from X currency to base EUR
                # USD -> EUR
                converted_value = value / cc.get_conversion_rate(
                    self.currency_conversion["db"][currency_from], conv_date
                )
            else:
                # currency_to != "EUR"
                if currency_from != "EUR":
                    # Convert from X1 currency to X2 currency
                    # X1 -> EUR
                    # EUR -> X2
                    conversion_rate_x1 = cc.get_conversion_rate(
                        self.currency_conversion["db"][currency_from], conv_date
                    )
                    conversion_rate_x2 = cc.get_conversion_rate(
                        self.currency_conversion["db"][currency_to], conv_date
                    )
                    converted_value = value / conversion_rate_x1 * conversion_rate_x2
                else:
                    # currency_from == "EUR"
                    # Convert from EUR to X currency
                    # EUR -> USD
                    converted_value = value * cc.get_conversion_rate(
                        self.currency_conversion["db"][currency_to], conv_date
                    )

        return converted_value

    def call_function_depending_on_shares_to_use(
        self, share_sell_date: str, function: Callable, function_param: Any
    ) -> None:
        """
        Call a function (passed as parameter) depending on "shares_to_use" and share sell date
        ("date_sell")
        """
        if self.shares_to_use == SharesToUse.ALL:
            function(function_param)
        elif self.shares_to_use == SharesToUse.ONLY_HOLDING and share_sell_date == "":
            function(function_param)
        elif self.shares_to_use == SharesToUse.ONLY_SOLD and share_sell_date != "":
            function(function_param)

    def get_total_proceeds(self) -> dict:
        """Get total proceeds"""
        total = {"x": 0}

        def sum_share_proceeds(share):
            total["x"] += share["proceeds"]

        for share in self.shares_list:
            self.call_function_depending_on_shares_to_use(
                share["date_sell"], sum_share_proceeds, share
            )

        return total["x"]

    def get_total_real_proceeds(self) -> dict:
        """Get total real proceeds"""
        total = {"x": 0}

        def sum_share_real_proceeds(share):
            total["x"] += share["proceeds_real"]

        for share in self.shares_list:
            self.call_function_depending_on_shares_to_use(
                share["date_sell"], sum_share_real_proceeds, share
            )

        return total["x"]

    def set_share_perc_per_total_proceeds(self, share: dict) -> None:
        """Set share percentages against total proceeds"""

        def set_share_perc(share):
            share["share_perc_per_total_proceeds"] = (
                share["proceeds"] / self.total_proceeds * 100
            )

        # Set default value
        share["share_perc_per_total_proceeds"] = 0

        # Chane share percentage depending on "shares_to_use"
        self.call_function_depending_on_shares_to_use(
            share["date_sell"], set_share_perc, share
        )

    def set_share_perc_per_total_real_proceeds(self, share: dict) -> float:
        """Set share percentages against total real proceeds"""

        def set_share_perc(share):
            share["share_perc_per_total_real_proceeds"] = (
                share["proceeds_real"] / self.total_real_proceeds * 100
            )

        # Set default value
        share["share_perc_per_total_real_proceeds"] = 0

        # Chane share percentage depending on "shares_to_use"
        self.call_function_depending_on_shares_to_use(
            share["date_sell"], set_share_perc, share
        )

    def get_total_proceeds_per_stock(self) -> dict:
        """Get total proceeds per stock"""
        total_proceeds_per_stock = {}

        def add_share_proceeds_to_stocks_list(share: dict):
            if share["symbol"] in total_proceeds_per_stock:
                total_proceeds_per_stock[share["symbol"]] += share["proceeds"]
            else:
                total_proceeds_per_stock[share["symbol"]] = share["proceeds"]

        for share in self.shares_list:
            self.call_function_depending_on_shares_to_use(
                share["date_sell"], add_share_proceeds_to_stocks_list, share
            )

        return total_proceeds_per_stock

    def get_total_real_proceeds_per_stock(self) -> dict:
        """Get total real proceeds per stock"""
        total_proceeds_per_stock = {}

        def add_share_proceeds_to_stocks_list(share: dict):
            if share["symbol"] in total_proceeds_per_stock:
                total_proceeds_per_stock[share["symbol"]] += share["proceeds_real"]
            else:
                total_proceeds_per_stock[share["symbol"]] = share["proceeds_real"]

        for share in self.shares_list:
            self.call_function_depending_on_shares_to_use(
                share["date_sell"], add_share_proceeds_to_stocks_list, share
            )

        return total_proceeds_per_stock

    def get_stock_perc_per_total_proceeds(self) -> dict:
        """
        Get stock percentages against total proceeds
        Note that `self.total_proceeds` and `self.total_proceeds_per_stock` are already calculated
        for desired `self.shares_to_use` and `self.currency`
        """
        stock_perc_per_total_proceeds = {}

        for stock_str, stock_proceeds in self.total_proceeds_per_stock.items():
            stock_perc_per_total_proceeds[stock_str] = (
                stock_proceeds / self.total_proceeds * 100
            )

        return stock_perc_per_total_proceeds

    def get_stock_perc_per_total_real_proceeds(self) -> dict:
        """
        Get stock percentages against total real proceeds
        Note that `self.total_proceeds` and `self.total_proceeds_per_stock` are already calculated
        for desired `self.shares_to_use` and `self.currency`
        """
        stock_perc_per_total_real_proceeds = {}

        for stock_str, stock_proceeds in self.total_real_proceeds_per_stock.items():
            stock_perc_per_total_real_proceeds[stock_str] = (
                stock_proceeds / self.total_real_proceeds * 100
            )

        return stock_perc_per_total_real_proceeds

    def get_stock_net_profit(self) -> dict:
        """Get net profit per stock"""
        stock_net_profit = {}

        def add_share_net_profit_to_stocks_list(share: dict):
            if share["symbol"] in stock_net_profit:
                stock_net_profit[share["symbol"]] += share["net_profit"]
            else:
                stock_net_profit[share["symbol"]] = share["net_profit"]

        for share in self.shares_list:
            self.call_function_depending_on_shares_to_use(
                share["date_sell"], add_share_net_profit_to_stocks_list, share
            )

        return stock_net_profit

    def get_total_net_profit(self) -> float:
        """Get total net profit for all the stocks"""
        total_sum = 0

        for stock_str, stock_net_profit in self.stock_net_profit.items():
            total_sum += stock_net_profit

        return total_sum

    def get_stock_investment(self) -> dict:
        """Get investment per stock"""
        stock_investment = {}

        def add_share_return_on_investment_to_stocks_list(share: dict):
            if share["symbol"] in stock_investment:
                stock_investment[share["symbol"]] += share["investment_total"]
            else:
                stock_investment[share["symbol"]] = share["investment_total"]

        for share in self.shares_list:
            self.call_function_depending_on_shares_to_use(
                share["date_sell"],
                add_share_return_on_investment_to_stocks_list,
                share,
            )

        return stock_investment

    def get_total_investment(self) -> float:
        """Get total investment for all the stocks"""
        total_sum = 0

        for stock_str, stock_investment in self.stock_investment.items():
            total_sum += stock_investment

        return total_sum

    def get_stock_return_on_investment(self) -> dict:
        """Get return on investment for individual stocks"""
        stock_return_on_investment = {}

        for stock_str, stock_investment in self.stock_investment.items():
            stock_return_on_investment[stock_str] = (
                self.stock_net_profit[stock_str] / stock_investment * 100
            )

        return stock_return_on_investment

    def get_total_return_on_investment(self) -> float:
        """Get total return on investment for all the stocks"""
        return self.total_net_profit / self.total_investment * 100

    def add_total_stock_proceeds_to_shares_list(self) -> list[dict]:
        """Add stock_proceeds to shares list"""

        def set_stock_proceeds_to_share(fn_param: tuple):
            # Expend tuple to individual variable
            (share, stock_proceeds) = fn_param
            share["stock_proceeds"] = stock_proceeds

        for stock_str, stock_proceeds in self.total_proceeds_per_stock.items():
            for share in self.shares_list:
                if stock_str == share["symbol"]:
                    # Set default value
                    share["stock_proceeds"] = 0

                    # Chane share percentage depending on "shares_to_use"
                    self.call_function_depending_on_shares_to_use(
                        share["date_sell"],
                        set_stock_proceeds_to_share,
                        (share, stock_proceeds),
                    )

        return self.shares_list

    def add_total_real_stock_proceeds_to_shares_list(self) -> list[dict]:
        """Add stock_proceeds_real to shares list"""

        def set_stock_real_proceeds_to_share(fn_param: tuple):
            # Expend tuple to individual variable
            (share, stock_proceeds) = fn_param
            share["stock_proceeds_real"] = stock_proceeds

        for stock_str, stock_proceeds in self.total_real_proceeds_per_stock.items():
            for share in self.shares_list:
                if stock_str == share["symbol"]:
                    # Set default value
                    share["stock_proceeds_real"] = 0

                    # Chane share percentage depending on "shares_to_use"
                    self.call_function_depending_on_shares_to_use(
                        share["date_sell"],
                        set_stock_real_proceeds_to_share,
                        (share, stock_proceeds),
                    )

        return self.shares_list

    def add_stock_perc_per_total_proceeds_to_shares_list(self) -> list[dict]:
        """Add stock percentages per total proceeds to shares list"""

        def set_stock_perc_per_total_proceeds(fn_param: tuple):
            # Expend tuple to individual variable
            (share, stock_proceeds) = fn_param
            share["stock_perc_per_total_proceeds"] = (
                stock_proceeds / self.total_proceeds * 100
            )

        for stock_str, stock_proceeds in self.total_proceeds_per_stock.items():
            for share in self.shares_list:
                if stock_str == share["symbol"]:
                    # Set default value
                    share["stock_perc_per_total_proceeds"] = 0

                    # Chane share percentage depending on "shares_to_use"
                    self.call_function_depending_on_shares_to_use(
                        share["date_sell"],
                        set_stock_perc_per_total_proceeds,
                        (share, stock_proceeds),
                    )

        return self.shares_list

    def add_stock_perc_per_total_real_proceeds_to_shares_list(self) -> list[dict]:
        """Add stock percentages per total real proceeds to shares list"""

        def set_stock_perc_per_total_real_proceeds(fn_param: tuple):
            # Expend tuple to individual variable
            (share, stock_proceeds) = fn_param
            share["stock_perc_per_total_real_proceeds"] = (
                stock_proceeds / self.total_real_proceeds * 100
            )

        for stock_str, stock_proceeds in self.total_real_proceeds_per_stock.items():
            for share in self.shares_list:
                if stock_str == share["symbol"]:
                    # Set default value
                    share["stock_perc_per_total_real_proceeds"] = 0

                    # Chane share percentage depending on "shares_to_use"
                    self.call_function_depending_on_shares_to_use(
                        share["date_sell"],
                        set_stock_perc_per_total_real_proceeds,
                        (share, stock_proceeds),
                    )

        return self.shares_list
