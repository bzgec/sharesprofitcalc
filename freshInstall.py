#!/usr/bin/env python

# Standard libraries
from shutil import copy2

# 3rd party packages
from rich import print

# Local source
from src.helperLib.helper import setup_files

setup_files("config")

################################################################################
# Mail sender configuration
################################################################################
with open("config/mail_sender.json", "w") as file:
    sendFromGmail = input("Enter gmail from which mails are send: ")
    gmailPass = input("Enter gmail password for mail above: ")
    sendToMail = input("Enter mail to which mails are send: ")

    file.write("{\n")
    file.write('    "senderMail": "' + sendFromGmail + '"\n')
    file.write('    "senderPass": "' + gmailPass + '"\n')
    file.write('    "sendTo": "' + sendToMail + '"\n')
    file.write("}\n")

################################################################################
# Shares list configuration
################################################################################
copy2("sharesListExample.py", "config/sharesList.py")
print('[bold]Edit "config/sharesList.py" to match your shares!')

################################################################################
# Docker container crontab configuration
################################################################################
copy2("crontabExample.txt", "config/crontab.txt")
print('[bold]Edit "config/crontab.txt" to set when docker should execute your script!')
